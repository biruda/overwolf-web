'use strict';
// modules
import {Component, OnDestroy, OnInit} from '@angular/core';
// configs
import {IMainConfig} from '../../../_system/configs/main.config';
import {INote, Note} from '../../../_system/models/release/Note';
import {App} from "../../../_system/models/apps/App";


export namespace IReleaseNoteComponent {

  export interface Data {
    activeAppID: string;
    notes: INote.Api.Item[];
    noteFields: INote.Options.FieldType[];
    noteFieldNames: INote.Options.FieldNames;
    newNote: {
      fields: NewNote.DataFilterFields;
      values: NewNote.DataFilterValues;
      errors: NewNote.DataFilterErrors,
      isChecked: NewNote.DataFilterIsChecked,
    };
  }

  namespace NewNote {

    export interface DataFilterFields extends INote.Options.NewAppModes {
      VersionNumber: 'VersionNumber';
      ReleaseNotes: 'ReleaseNotes';
      ReleaseDate: 'ReleaseDate';
    }

    export interface DataFilterValues extends INote.Options.NewAppModes {
      VersionNumber: number;
      ReleaseNotes: string;
      ReleaseDate: string;
    }

    export interface DataFilterErrors extends INote.Options.NewAppModes {
      VersionNumber: string[];
      ReleaseNotes: string[];
      ReleaseDate: string[];
    }

    export interface DataFilterIsChecked extends INote.Options.NewAppModes {
      VersionNumber: boolean;
      ReleaseNotes: boolean;
      ReleaseDate: boolean;
    }
  }
}

@Component({
  templateUrl: './release-note.component.html',
  styleUrls: ['./release-note.component.scss']
})
export class ReleaseNoteComponent implements OnInit, OnDestroy {
  // internal static
  public lang: IMainConfig.LangField;
  public data: IReleaseNoteComponent.Data = {
    activeAppID: null,
    notes: [],
    noteFields: INote.Options.$FieldList,
    noteFieldNames: {
      AppID: 'AppID',
      VersionNumber: 'VersionNumber',
      ReleaseDate: 'ReleaseDate',
      ReleaseNotes: 'ReleaseNotes',
      Published: 'Published',
    },
    newNote: {
      fields: {
        VersionNumber: 'VersionNumber',
        ReleaseNotes: 'ReleaseNotes',
        ReleaseDate: 'ReleaseDate',
      },
      values: {
        VersionNumber: null,
        ReleaseNotes: null,
        ReleaseDate: null,
      },
      errors: {
        VersionNumber: [],
        ReleaseNotes: [],
        ReleaseDate: [],
      },
      isChecked: {
        VersionNumber: false,
        ReleaseNotes: false,
        ReleaseDate: false,
      },
    },
  };

  constructor() {
  }

  ngOnInit() {
    this.data.activeAppID = App.getActiveApp();
  }

  ngOnDestroy() {
  }

  setNote(value, field) {
    this.data.newNote.values[field] = value;
  }

  sendNote() {
    Note.sendNew({
      AppID: this.data.activeAppID,
      VersionNumber: this.data.newNote.values.VersionNumber,
      ReleaseDate: new Date(this.data.newNote.values.ReleaseDate),
      ReleaseNotes: this.data.newNote.values.ReleaseNotes,
      Published: false,
    });
    this.data.newNote.values = {
      VersionNumber: null,
      ReleaseNotes: null,
      ReleaseDate: null,
    };
  }

  updateNote(noteID: string, isPublished: boolean) {
    Note.sendUpdate({
      _id: noteID,
      Published: isPublished,
    });
    this.data.newNote.values = {
      VersionNumber: null,
      ReleaseNotes: null,
      ReleaseDate: null,
    };
  }

}
