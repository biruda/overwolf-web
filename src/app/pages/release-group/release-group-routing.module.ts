'use strict';
// modules
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// components
import {ReleaseNoteComponent} from './note/release-note.component';
// configs
import {getUrlConfig, IUrlConfig} from '../../_system/configs/url.config';
// included configs
const urlConfig: IUrlConfig.Config = getUrlConfig();
// main route config
const routes: Routes = [
  {
    path: urlConfig.releaseGroup.note.url,
    component: ReleaseNoteComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReleaseGroupRoutingModule {
}
