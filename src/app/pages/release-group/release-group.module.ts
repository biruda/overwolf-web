'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
// components
import {ComponentsTableModule} from '../../components/table/table.module';
import {ComponentsControlModule} from '../../components/control/control.module';
// pages
import {ReleaseNoteComponent} from './note/release-note.component';
// routes
import {ReleaseGroupRoutingModule} from './release-group-routing.module';

@NgModule({
  imports: [
    CommonModule, HttpClientModule,
    ReleaseGroupRoutingModule,
    ComponentsTableModule, ComponentsControlModule
  ],
  providers: [],
  declarations: [
    ReleaseNoteComponent
  ]
})
export class ReleaseGroupModule {
}
