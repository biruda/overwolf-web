'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
// components
import {ComponentsTableModule} from '../../components/table/table.module';
import {ComponentsControlModule} from '../../components/control/control.module';
// pages
import {AppsAppComponent} from './app-list/apps-app.component';
// routes
import {AppsGroupRoutingModule} from './apps-group-routing.module';

@NgModule({
  imports: [
    CommonModule, HttpClientModule,
    AppsGroupRoutingModule,
    ComponentsTableModule, ComponentsControlModule
  ],
  providers: [],
  declarations: [
    AppsAppComponent
  ]
})
export class AppsGroupModule {
}
