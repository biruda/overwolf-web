'use strict';
// modules
import {Component, OnInit} from '@angular/core';
// services
import {RoutingService} from '../../../_system/services/routing.service';
// models
import {App, IApp} from '../../../_system/models/apps/App';
// configs
import {IMainConfig} from '../../../_system/configs/main.config';
import {getUrlConfig, IUrlConfig} from '../../../_system/configs/url.config';


export namespace IAppsAppComponent {

  export interface Data {
    apps: IApp.Api.Item[];
    appsFields: IApp.Options.FieldType[];
    appsFieldNames: IApp.Options.FieldNames;
    newApp: {
      fields: DataFilterFields;
      values: DataFilterValues;
      errors: DataFilterErrors,
      isChecked: DataFilterIsChecked,
    };
  }

  interface DataFilterFields extends IApp.Options.NewAppModes {
    AppName: 'AppName';
  }

  export interface DataFilterValues extends IApp.Options.NewAppModes {
    AppName: string;
  }

  interface DataFilterErrors extends IApp.Options.NewAppModes {
    AppName: string[];
  }

  interface DataFilterIsChecked extends IApp.Options.NewAppModes {
    AppName: boolean;
  }

  export interface Configs {
    _urlConfig: IUrlConfig.Config;
  }
}

@Component({
  templateUrl: './apps-app.component.html',
  styleUrls: ['./apps-app.component.scss']
})
export class AppsAppComponent implements OnInit {
  public lang: IMainConfig.LangField;
  public data: IAppsAppComponent.Data = {
    apps: [],
    appsFields: IApp.Options.$FieldList,
    appsFieldNames: {
      AppName: `app' name`,
      Author: `author' id`,
      created_at: `created at`,
    },
    newApp: {
      fields: {
        AppName: 'AppName',
      },
      values: {
        AppName: null,
      },
      errors: {
        AppName: [],
      },
      isChecked: {
        AppName: false,
      },
    },
  };
  public configs: IAppsAppComponent.Configs = {
    _urlConfig: getUrlConfig(),
  };

  constructor() {
  }

  ngOnInit() {
    this.getApps();
  }

  getApps(): void {
    this.data.apps = App.get();
  }

  // form

  setApp(value, field) {
    this.data.newApp.values[field] = value;
  }

  sendApp() {
    App.send(this.data.newApp.values);
    this.data.newApp.values = {
      AppName: null
    };
  }

  // routing

  goToUserTracks(appID: string): void {
    App.setActiveApp(appID);

    const routeItem: IUrlConfig.ConfigItem = this.configs._urlConfig.releaseGroup.note;
    RoutingService.setAppRender(routeItem.url);
  }
}
