'use strict';
// modules
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// components
import {AppsAppComponent} from './app-list/apps-app.component';
// configs
import {getUrlConfig, IUrlConfig} from '../../_system/configs/url.config';
// included configs
const urlConfig: IUrlConfig.Config = getUrlConfig();
// main route config
const routes: Routes = [
  {
    path: urlConfig.appGroup.appList.url,
    component: AppsAppComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppsGroupRoutingModule {
}
