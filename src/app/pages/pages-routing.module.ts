'use strict';
// modules
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// configs
import {getUrlConfig, IUrlConfig} from '../_system/configs/url.config';
// included configs
const urlConfig: IUrlConfig.Config = getUrlConfig();
// main route config
const routes: Routes = [
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: urlConfig.appGroup.appList.url,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
  constructor() {
  }
}
