'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
// pages-groups
import {AppsGroupModule} from './apps-group/apps-group.module';
import {ReleaseGroupModule} from './release-group/release-group.module';
import {UsersGroupModule} from './users-group/users-group.module';
// routes
import {PagesRoutingModule} from './pages-routing.module';
// services
import {ApiService} from '../_system/services/api.service';

@NgModule({
  imports: [
    CommonModule, FormsModule, HttpClientModule,
    AppsGroupModule, ReleaseGroupModule, UsersGroupModule,
    PagesRoutingModule
  ],
  providers: [
    ApiService
  ],
})
export class PagesModule {
}
