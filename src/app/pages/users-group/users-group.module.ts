'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
// pages
import {UsersSingComponent} from './sing/users-sing.component';
// routes
import {UsersGroupRoutingModule} from './users-group-routing.module';

@NgModule({
  imports: [
    CommonModule, HttpClientModule,
    UsersGroupRoutingModule
  ],
  providers: [],
  declarations: [
    UsersSingComponent
  ]
})
export class UsersGroupModule {
}
