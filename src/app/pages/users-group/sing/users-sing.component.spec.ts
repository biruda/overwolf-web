import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AdminAuthSingInComponent} from './users-sing.component';

describe('AdminAuthSingInComponent', () => {
  let component: AdminAuthSingInComponent;
  let fixture: ComponentFixture<AdminAuthSingInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminAuthSingInComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAuthSingInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
