'use strict';
// modules
import {Component, OnInit, ViewChild} from '@angular/core';
// services
import {ApiService} from '../../../_system/services/api.service';
import {NotificationService} from '../../../_system/services/modal.service';
// models
import {IUser, User} from '../../../_system/models/users/User';
// configs
import {getNotifyConfig, INotifyConfig} from '../../../_system/configs/notify.config';
import {getUrlConfig, IUrlConfig} from '../../../_system/configs/url.config';

declare const gapi: any;

export namespace IUsersSingComponent {

  export namespace Info {

    export interface GoogleUser {
      getAuthResponse(): { id_token: string };

      getBasicProfile(): {
        getId(): string;
        getName(): string;
        getImageUrl(): string;
        getEmail(): string;
      };

      isSignedIn(): boolean;
    }

    export interface GoogleProfile {
      getId(): string;

      getName(): string;

      getImageUrl(): string;

      getEmail(): string;
    }

    export interface GoogleAccountParams {
      El: string;
      Zi: {
        access_token: string;
        expires_at: number;
        expires_in: number;
        first_issued_at: number;
        id_token: string; // need for api
        idpId: string;
        login_hint: string;
        scope: string;
        session_state: {
          extraQueryParams: {
            authuser: string;
          };
        };
        token_type: string;
      };
      w3: {
        Eea: string;
        Paa: string;
        U3: string; // email
        ig: string; // full name
        ofa: string; // first name
        wea: string; // last name
      };
    }
  }

  export interface Configs {
    _urlConfig: IUrlConfig.Config;
    _notifyConfig: INotifyConfig.Config;
    isGoogleAuthLoad: boolean;
  }

  export interface Models {
    user: User;
  }
}

@Component({
  templateUrl: './users-sing.component.html',
  styleUrls: ['./users-sing.component.scss']
})
export class UsersSingComponent implements OnInit {
  public configs: IUsersSingComponent.Configs = {
    _urlConfig: getUrlConfig(),
    _notifyConfig: getNotifyConfig(),
    isGoogleAuthLoad: false,
  };
  private _models: IUsersSingComponent.Models = {
    user: new User(),
  };

  @ViewChild('googleBtn') domSocialGoogleSignIn;
  @ViewChild('googleBtnManual') domSocialGoogleSignInManual;

  constructor(// private _router: Router,
    private _api: ApiService) {

  }

  ngOnInit() {
    // check if already logged
    const token: string | null = User.getToken();
    if (token !== null) {
      // route to overview
      this.goToDashboard();
      return;
    }

    this.googleInit();

    const params: object = User.getHashParams();
    if (params !== null && params['id_token'] !== void 0) {
      return this.validateAdmin(params['id_token']);
    }

    // auto - auth
    this.autoAuth();
  }

  autoAuth() {
    if (this.domSocialGoogleSignIn === void 0 || this.domSocialGoogleSignIn.nativeElement === void 0) {
      return setTimeout(() => this.autoAuth(), 50);
    }

    this.domSocialGoogleSignIn.nativeElement.click();
  }

  googleInit() {
    // this.googleInitLoad();
    this.googleInitRender();
  }

  googleInitLoad() {
    // check is lib loaded
    if (typeof gapi === 'undefined' || this.domSocialGoogleSignIn === void 0 || this.domSocialGoogleSignIn.nativeElement === void 0) {
      return setTimeout(() => this.googleInit(), 50);
    }

    // load lib' methods
    gapi.load('auth2', () => {

      // // get correct clientID
      // const clientID = window.location.href.indexOf('localhost') !== -1
      //   ? '6465121995-2ugrrq1db9vckihuh8gool5t8k5p1p8k.apps.googleusercontent.com'
      //   : '6465121995-bbcmu02b9ck44l7l1k4ufgeb9n68pvmi.apps.googleusercontent.com';

      // get correct clientID
      const clientID = '6465121995-bbcmu02b9ck44l7l1k4ufgeb9n68pvmi.apps.googleusercontent.com';

      // params should be equal to app-list.html in meta
      gapi.auth2.init({
        client_id: clientID,
        // client_id: '6465121995-2ugrrq1db9vckihuh8gool5t8k5p1p8k.apps.googleusercontent.com', // dev
        // client_id: '6465121995-bbcmu02b9ck44l7l1k4ufgeb9n68pvmi.apps.googleusercontent.com', // prod
        cookiepolicy: 'single_host_origin',
        scope: 'profile email',
        ux_mode: 'redirect'
      }).attachClickHandler(this.domSocialGoogleSignIn.nativeElement, {},
        (googleUser: IUsersSingComponent.Info.GoogleUser) => {
          const profile: IUsersSingComponent.Info.GoogleProfile = googleUser.getBasicProfile();
          const token: string = googleUser.getAuthResponse().id_token;
          // const email: string = profile.getEmail();

          this.validateAdmin(token);
        }, (error) => {
          NotificationService.sendUpdates({content: [{info: error.error}]});
          // Notification doesn't work inside handler of lib
          alert(error.error);
        });

      // auto - auth
      // this.domSocialGoogleSignIn.nativeElement.click();
    });
  }

  googleInitRender() {
    // check is lib loaded
    if (typeof gapi === 'undefined'
      || this.domSocialGoogleSignInManual === void 0
      || this.domSocialGoogleSignInManual.nativeElement === void 0
    ) {
      return setTimeout(() => this.googleInit(), 50);
    }

    gapi.signin2.render('googleBtnManual', {
      scope: 'profile email',
      width: 280,
      height: 60,
      longtitle: true,
      // theme: 'dark',
      theme: 'light',
      ux_mode: 'redirect',
      onsuccess: (data: IUsersSingComponent.Info.GoogleAccountParams) => {
        this.validateAdmin(data.Zi.id_token);
      },
    });
  }

  private validateAdmin(googleToken: string) {
    const dataRequest: IUser.Api.AuthRequest = {googleToken};
    this._api.post(this._models.user.model, '', dataRequest, (err, data: [IUser.Api.AuthResponse]) => {
      // change put to post - not create, but just singIn
      // this._api.put(this._models.user.model, '', dataRequest, (err, data: [IUser.Api.AuthResponse]) => {
      if (err !== null) {
        // handler errors from servers
        NotificationService.sendUpdates({content: [{info: err}]});
        // Notification doesn't work inside handler of lib
        alert(err);
        return;
      }

      User.set(data[0]);

      // this.goToDashboardReload();
      this.goToDashboard();
    }, true);
  }

  /**
   * @function goToDashboard
   */
  goToDashboard() {
    // todo
    // this._router.navigate(['/' + this.configs._urlConfig.statisticsGroup.overview.url]).then();
  }

  // /**
  //  * @function goToDashboardReload
  //  */
  // goToDashboardReload() {
  //   window.location.href = this.configs._urlConfig.adminGroup.app-list.url;
  // }
}
