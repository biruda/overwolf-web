'use strict';
// modules
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// components
import {UsersSingComponent} from './sing/users-sing.component';
// configs
import {getUrlConfig, IUrlConfig} from '../../_system/configs/url.config';
// included configs
const urlConfig: IUrlConfig.Config = getUrlConfig();
// main route config
const routes: Routes = [
  {
    path: urlConfig.usersGroup.sign.url,
    component: UsersSingComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersGroupRoutingModule {
}
