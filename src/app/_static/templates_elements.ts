'use strict';
// interfaces
import {IMainConfig} from '../_system/configs/main.config';


/**
 * @namespace ITemplatesElements
 */
export namespace ITemplatesElements {

  /**
   * @interface Info
   */
  export interface Info {
    confirmTitle: string;
    actions: {
      choose: string,
      profile: string,
      finish: string,
      deletePhoto: string,
      remove: string,
      cancel: string,
    };
    labels: {
      terms: string,
      birth: {
        day: string,
        month: string,
        year: string,
      }
    };
  }

  /**
   * @interface Configs
   * @extends IMainConfig.LangContent
   * @param {IErrors.Info} en
   * @param {IErrors.Info} se
   */
  export interface Configs extends IMainConfig.LangContent {
    en: ITemplatesElements.Info;
    se: ITemplatesElements.Info;
  }
}

/**
 * @const _templatesElementsStatic
 * @description base data for all template' components
 * @type {ITemplatesElements.Configs}
 * @private
 */
const _templatesElementsStatic: ITemplatesElements.Configs = {
  en: {
    confirmTitle: 'Are you sure you want do delete this photo?',
    actions: {
      choose: 'choose it',
      profile: 'View profile',
      deletePhoto: 'Delete this photo',
      remove: 'Delete',
      cancel: 'Cancel',
      finish: 'Finish',
    },
    labels: {
      terms: 'I accept the terms and conditions contained in BlindDater',
      birth: {
        day: 'day',
        month: 'month',
        year: 'year',
      },
    },
  },
  se: {
    confirmTitle: 'Are you sure you want do delete this photo?',
    actions: {
      choose: 'välj det',
      profile: 'Visa profil',
      finish: 'Slutför',
      deletePhoto: 'Delete this photo',
      remove: 'Delete',
      cancel: 'Cancel',
    },
    labels: {
      terms: 'Jag accepterar de allmänna villkor som finns hos BlindDater',
      birth: {
        day: 'dag',
        month: 'månad',
        year: 'år',
      },
    },
  },
};

/**
 * @function getTemplatesElementsStatic
 * @description get exemplar of configs
 * @param {IMainConfig.LangField} lang
 * @return {ITemplatesElements.Info}
 */
export function getTemplatesElementsStatic(lang: IMainConfig.LangField): ITemplatesElements.Info {
  if (_templatesElementsStatic[lang] === void 0) {
    return _templatesElementsStatic.en;
  }

  return _templatesElementsStatic[lang];
}
