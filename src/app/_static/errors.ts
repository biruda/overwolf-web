'use strict';
// interfaces
import {IMainConfig} from '../_system/configs/main.config';


/**
 * @const _macros
 * @description use for change error' body
 * @type {string}
 * @private
 */
export const _macros = '%%';

/**
 * @namespace IErrors
 */
export namespace IErrors {

  /**
   * @interface Info
   * @param {string} isSending
   * @param {string} notAllowed
   * @param {string} notExist
   * @param {string} wrong
   * @param {string} contain
   * @param {string} require
   * @param {string} requireSimple
   * @param {string} limitLess
   * @param {string} limitMore
   * @param {string} notSecure
   */
  export interface Info {
    isSending: string;
    notAllowed: string;
    notExist: string;
    notEqualPass: string;
    wrong: string;
    contain: string;
    require: string;
    requireSimple: string;
    limitLess: string;
    limitMore: string;
    limitLessMeasure: string;
    limitMoreMeasure: string;
    notSecure: string;
    isRemoved: string;
    hourRoom: {
      nowIsNotActive: string,
      nowIsEnded: string,
    };
  }

  /**
   * @interface Configs
   * @extends IMainConfig.LangContent
   * @param {IErrors.Info} en
   * @param {IErrors.Info} se
   */
  export interface Configs extends IMainConfig.LangContent {
    en: IErrors.Info;
    se: IErrors.Info;
  }
}

/**
 * @const _Errors
 * @description list of errors used in validation
 * @type {IErrors.Configs}
 * @private
 */
const _Errors: IErrors.Configs = {
  en: {
    isSending: `form is sending now`,
    notAllowed: `Value not allowed`,
    notExist: `Option ${_macros} not exist`,
    notEqualPass: `Passwords are not equal`,
    wrong: `Wrong ${_macros}`,
    contain: `Should contain at least one ${_macros}`,
    require: `Should entered ${_macros}`,
    requireSimple: `is required`,
    limitLess: `Should be less than ${_macros} symbols`,
    limitMore: `Should be more than ${_macros} symbols`,
    limitLessMeasure: `Should be less than ${_macros} `,
    limitMoreMeasure: `Should be more than ${_macros} `,
    notSecure: `Please login again`,
    isRemoved: `Your account is removed`,
    hourRoom: {
      nowIsNotActive: 'Blinddate Room is not open now',
      nowIsEnded: 'Blinddate Hour is finish now',
    },
  },
  se: {
    isSending: `Formuläret skickat`,
    notAllowed: `Ogiltig information`,
    notExist: `Alternativet ${_macros} existerar inte`,
    notEqualPass: `Lösenorden matchar inte`,
    wrong: `Fel ${_macros}`,
    contain: `Bör innehålla minst en ${_macros}`,
    require: `Ska komma in ${_macros}`,
    requireSimple: `krävs`,
    limitLess: `Bör vara mindre än ${_macros} tecken`,
    limitMore: `Bör vara mer än ${_macros} tecken`,
    limitLessMeasure: `Bör vara mindre än ${_macros} `,
    limitMoreMeasure: `Bör vara mer än ${_macros} `,
    notSecure: `Vänligen logga in igen`,
    isRemoved: `Your account is removed`,
    hourRoom: {
      nowIsNotActive: 'Blinddaterummet är inte öppet nu',
      nowIsEnded: 'Blinddatetimmen är slut nu',
    },
  },
};

/**
 * @function getErrors
 * @description get exemplar of configs
 * @param {IMainConfig.LangField} lang
 * @return {IErrors.Info}
 */
export function getErrors(lang: IMainConfig.LangField): IErrors.Info {
  if (_Errors[lang] === void 0) {
    return _Errors.en;
  }

  return _Errors[lang];
}

/**
 * @function getErrorsFull
 * @description get exemplar of configs
 * @return {IErrors.Configs}
 */
export function getErrorsFull(): IErrors.Configs {
  return _Errors;
}
