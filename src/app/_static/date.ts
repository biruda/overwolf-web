'use strict';
// interfaces
import {IMainConfig} from '../_system/configs/main.config';


/**
 * @interface IMonths
 * @private
 */
export namespace IMonths {

  /**
   * @interface Info
   */
  export interface Info {
    long: {
      1: string,
      2: string,
      3: string,
      4: string,
      5: string,
      6: string,
      7: string,
      8: string,
      9: string,
      10: string,
      11: string,
      12: string,
    };
    short: {
      1: string,
      2: string,
      3: string,
      4: string,
      5: string,
      6: string,
      7: string,
      8: string,
      9: string,
      10: string,
      11: string,
      12: string,
    };
  }

  /**
   * @interface Config
   */
  export interface Config extends IMainConfig.LangContent {
    en: IMonths.Info;
    se: IMonths.Info;
  }

}

/**
 *
 * @type {IMonths.Config}
 * @private
 */
const _Months: IMonths.Config = {
  en: {
    long: {
      1: 'January',
      2: 'February',
      3: 'March',
      4: 'April',
      5: 'May',
      6: 'June',
      7: 'July',
      8: 'August',
      9: 'September',
      10: 'October',
      11: 'November',
      12: 'December',
    },
    short: {
      1: 'Jan',
      2: 'Feb',
      3: 'Mar',
      4: 'Apr',
      5: 'May',
      6: 'Jun',
      7: 'Jul',
      8: 'Aug',
      9: 'Sep',
      10: 'Oct',
      11: 'Nov',
      12: 'Dec',
    }
  },
  se: {
    long: {
      1: 'Januari',
      2: 'Februari',
      3: 'Mars',
      4: 'April',
      5: 'Maj',
      6: 'Juni',
      7: 'Juli',
      8: 'Augusti',
      9: 'September',
      10: 'Oktober',
      11: 'November',
      12: 'December',
    },
    short: {
      1: 'Jan',
      2: 'Feb',
      3: 'Mar',
      4: 'Apr',
      5: 'Maj',
      6: 'Jun',
      7: 'Jul',
      8: 'Aug',
      9: 'Sep',
      10: 'Oct',
      11: 'Nov',
      12: 'Dec',
    }
  }
};

/**
 * @function getMonths
 * @description get exemplar of configs
 * @param {IMainConfig.LangField} lang
 * @return {IMonths.Info}
 */
export function getMonths(lang: IMainConfig.LangField): IMonths.Info {
  if (_Months[lang] === void 0) {
    return _Months.en;
  }

  return _Months[lang];
}

/**
 * @function getMonthsFull
 * @description get exemplar of configs all languages
 * @return {IMonths.Config}
 */
export function getMonthsFull(): IMonths.Config {
  return _Months;
}
