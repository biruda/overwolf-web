'use strict';
// interfaces
import {IMainConfig} from '../_system/configs/main.config';


/**
 * @interface IProfileStatic
 * @private
 */
export namespace IProfileStatic {

  /**
   * @interface Character
   * @todo: add model
   */
  export interface Character {
    happy: string;
    thoughtful: string;
    shy: string;
    generous: string;
    humble: string;
    frisky: string;
    realist: string;
    feet_on_ground: string;
    absend_minded: string;
    poetic: string;
    close_to_laughter: string;
    fragile: string;
    clown: string;
    skin_on_nose: string;
    hyperactive: string;
    pedantic: string;
    beskymmerlos: string;
    carelessness: string;
    couch_potato: string;
    Friluftmanniska: string;
    gasbag: string;
    outward: string;
    stubborn: string;
    ambitious: string;
    quiet: string;
    snoopy: string;
    jealous: string;
    sleepy: string;
    Familjekar: string;
    animal_lover: string;
    questioning: string;
    partyprisse: string;
    eager_beaver: string;
  }

  /**
   * @interface Config
   */
  export interface ConfigTraits extends IMainConfig.LangContent {
    en: IProfileStatic.Character;
    se: IProfileStatic.Character;
  }
}

/**
 * @const _profileTraits
 * @description translations for traits
 * @type {IProfileStatic.ConfigTraits}
 * @private
 */
const _profileTraits: IProfileStatic.ConfigTraits = {
  en: {
    happy: 'Happy',
    thoughtful: 'Thoughtful',
    shy: 'Shy',
    generous: 'Generous',
    humble: 'Humble',
    frisky: 'Frisky',
    realist: 'Realist',
    feet_on_ground: 'Feet on ground',
    absend_minded: 'Absend minded',
    poetic: 'Poetic',
    close_to_laughter: 'Close to laughter',
    fragile: 'Fragile',
    clown: 'Clown',
    skin_on_nose: 'Skin on nose',
    hyperactive: 'Hyperactive',
    pedantic: 'Pedantic',
    beskymmerlos: 'Carefree',
    carelessness: 'Carelessness',
    couch_potato: 'Couch potato',
    Friluftmanniska: 'Outdoorsman',
    gasbag: 'Gasbag',
    outward: 'Outward',
    stubborn: 'Stubborn',
    ambitious: 'Ambitious',
    quiet: 'Quiet',
    snoopy: 'Snoopy',
    jealous: 'Jealous',
    sleepy: 'Sleepy',
    Familjekar: 'Domesticity',
    animal_lover: 'Animal lover',
    questioning: 'Questioning',
    partyprisse: 'Party animal',
    eager_beaver: 'Workaholic',
  },
  se: {
    happy: 'Glad',
    thoughtful: 'Omtänksam',
    shy: 'Blyg',
    generous: 'Generös',
    humble: 'Ödmjuk',
    frisky: 'Sprallig',
    realist: 'Realist',
    feet_on_ground: 'Har fötterna på jorden',
    absend_minded: 'Tankspridd',
    poetic: 'Poet',
    close_to_laughter: 'Nära till skratt',
    fragile: 'Ömtålig',
    clown: 'Clown',
    skin_on_nose: 'Skinn på näsan',
    hyperactive: 'Hyperaktiv',
    pedantic: 'Pedant',
    beskymmerlos: 'Bekymmerslös',
    carelessness: 'Slarvpelle',
    couch_potato: 'Soffpotatis',
    Friluftmanniska: 'Friluftsmänniska',
    gasbag: 'Pratkvarn',
    outward: 'Utåtriktad',
    stubborn: 'Envis / Vinnarskalle',
    ambitious: 'Ambitiös',
    quiet: 'Lugn',
    snoopy: 'Nyfiken i en strut',
    jealous: 'Svartsjuk',
    sleepy: 'Sömnig',
    Familjekar: 'Familjekär',
    animal_lover: 'Djurvän',
    questioning: 'Ifrågasättande',
    partyprisse: 'Partyprisse',
    eager_beaver: 'Arbetsmyra',
  },
};

/**
 * @function getProfileTraits
 * @description get exemplar of configs
 * @param {IMainConfig.LangField} lang
 * @return {IProfileStatic.Character}
 */
export function getProfileTraits(lang: IMainConfig.LangField): IProfileStatic.Character {
  if (_profileTraits[lang] === void 0) {
    return _profileTraits.en;
  }

  return _profileTraits[lang];
}

/**
 * @function getProfileTraitsFull
 * @description get exemplar of configs
 * @return {IProfileStatic.ConfigTraits}
 */
export function getProfileTraitsFull(): IProfileStatic.ConfigTraits {
  return _profileTraits;
}
