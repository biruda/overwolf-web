'use strict';
// interfaces
import {IMainConfig} from '../_system/configs/main.config';


/**
 * @namespace ITemplatesBase
 */
export namespace ITemplatesBase {

  /**
   * @interface Info
   * @param {string} isSending
   * @param {string} notAllowed
   * @param {string} notExist
   * @param {string} wrong
   * @param {string} contain
   * @param {string} require
   * @param {string} requireSimple
   * @param {string} limitLess
   * @param {string} limitMore
   * @param {string} notSecure
   */
  export interface Info {
    apps: {
      title: string,
      androidAct: string,
      iosAct: string,
    };
    actions: {
      logOut: string,
      login: string,
      upgrade: string,
      testNotExist: string,
      minimize: string,
      closeMenu: string,
      getInTouch: string,
    };
    user: {
      typeRegular: string,
      typePremium: string,
    };
    blocks: {
      favorites: string,
      visitors: string,
      likes: string,
      messages: string,
      online: string,
      matchTitle: string,
    };
    logged: {
      last: string,
      seconds: string,
      minutes: string,
      hours: string,
      days: string,
      months: string,
    };
    counters: {
      toReleaseA: string,
      toReleaseB: string,
    };
  }

  /**
   * @interface Configs
   * @extends IMainConfig.LangContent
   * @param {IErrors.Info} en
   * @param {IErrors.Info} se
   */
  export interface Configs extends IMainConfig.LangContent {
    en: ITemplatesBase.Info;
    se: ITemplatesBase.Info;
  }
}

/**
 * @const _templatesBaseStatic
 * @description base data for all template' components
 * @type {ITemplatesBase.Configs}
 * @private
 */
const _templatesBaseStatic: ITemplatesBase.Configs = {
  en: {
    apps: {
      title: `Get the app now`,
      androidAct: `Download from Google Play`,
      iosAct: `Download from App Store`
    },
    actions: {
      logOut: `Log out`,
      login: `Log in`,
      upgrade: `Upgrade to Premium`,
      testNotExist: `Take the personality test and get daily matches!`,
      minimize: 'Minimize',
      closeMenu: 'Close menu',
      getInTouch: 'Get in touch',
    },
    user: {
      typeRegular: 'Freemium',
      typePremium: 'Premium',
    },
    blocks: {
      favorites: `Favorites`,
      visitors: `Visitors`,
      likes: `Likes`,
      messages: `Open messages`,
      online: `Online now`,
      matchTitle: 'Blinddate Matches',
    },
    logged: {
      last: 'Last seen ',
      seconds: ' seconds ago',
      minutes: ' minutes ago',
      hours: ' hours ago',
      days: ' days ago',
      months: ' months ago',
    },
    counters: {
      toReleaseA: 'Days left',
      toReleaseB: 'Until Release'
    }
  },
  se: {
    apps: {
      title: `Ladda ner vår App`,
      androidAct: `Ladda ner från Google Play`,
      iosAct: `Ladda ner från App Store`
    },
    actions: {
      logOut: `Logga ut`,
      login: `Logga in`,
      upgrade: `Uppgradera till Premium`,
      testNotExist: `Gör personlighetstestet och få nya personliga matchningar varje dag!`,
      minimize: 'Minimera',
      closeMenu: 'Nära menyn',
      getInTouch: 'Ta kontakt',
    },
    user: {
      typeRegular: 'Freemium',
      typePremium: 'Premium',
    },
    blocks: {
      favorites: `Favoriter`,
      visitors: `Besökare`,
      likes: `Gillar dig`,
      messages: `Meddelanden`,
      online: `Online nu`,
      matchTitle: 'Blinddate Match',
    },
    logged: {
      last: 'Senast sedd för ',
      seconds: ' sekunder sedan',
      minutes: ' minuter sedan',
      hours: ' timmar sedan',
      days: ' dagar sedan',
      months: ' månader sedan'
    },
    counters: {
      toReleaseA: 'Dagar kvar',
      toReleaseB: 'Till lansering',
    }
  },
};

/**
 * @function getTemplatesBaseStatic
 * @description get exemplar of configs
 * @param {IMainConfig.LangField} lang
 * @return {ITemplatesBase.Info}
 */
export function getTemplatesBaseStatic(lang: IMainConfig.LangField): ITemplatesBase.Info {
  if (_templatesBaseStatic[lang] === void 0) {
    return _templatesBaseStatic.en;
  }

  return _templatesBaseStatic[lang];
}
