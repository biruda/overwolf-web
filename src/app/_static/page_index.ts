'use strict';
// interfaces
import {IMainConfig} from '../_system/configs/main.config';


/**
 * @interface IIndexStatic
 * @private
 */
export namespace IIndexStatic {

  /**
   * @interface Info
   */
  export interface Info {
    title: string;
    main: {
      info: string[],
    };
    options: {
      title: {
        a: string,
        b: string
      },
      pairs: {
        a: string,
        b: string,
      }[]
    };
  }

  /**
   * @interface Config
   */
  export interface Config extends IMainConfig.LangContent {
    en: IIndexStatic.Info;
    se: IIndexStatic.Info;
  }
}

/**
 * @const _pageIndexStatic
 * @description main static for all components
 * @type {IIndexStatic.Config}
 * @private
 */
const _pageIndexStatic: IIndexStatic.Config = {
  en: {
    title: 'Index',
    main: {
      info: [
        `Is online dating new ground for you? Then it is perhaps not entirely obvious how to go about it and what `
        + `way that might be the best to make contact with other people.`,
        `Rules that are carved in stone does not really exist when it comes to online dating. However, there are `
        + `certain things that are important to consider regarding what to do so that the contact will become `
        + `interested in you.`,
        `Today, around 25% of Swedes meet their partners through online dating, and our goal is to contribute to `
        + `that number and help even more people find their true love. Thus, tips and advice can be appreciated. We`
        + ` would therefore like to offer some experiences we have noted from previous member's feedback:`,
      ],
    },
    options: {
      title: {
        a: 'Do',
        b: 'Don\'t'
      },
      pairs: [
        {
          a: `Just take contact with the members you actually want to know after having gone through their profiles`
          + `. It will ultimately make it easier for everyone, and you use your time right. In the long term, it will`
          + ` help other members who presented themselves insufficient to change their approach and actually get `
          + `noticed by other members.`,
          b: `Do not start "Spamming" several members with the same standard content. It will most likely lead to`
          + ` irritation and your chance for a great start can be lost.`
        }, {
          a: `Keep a cool tone to the one you write with and you will get so much more back. The same applies to `
          + `the response you get. If it doesn't feel right, then it's obviously not something you should allow.`,
          b: `Approach with care and consideration and do not approach anyone as an arrogant jerk. It is neither `
          + `something that will take you anywhere, nor something we allow.`
        }, {
          a: `Keep a cool tone to the one you write with and you will get so much more back. The same applies to `
          + `the response you get. If it doesn't feel right, then it's obviously not something you should allow.`,
          b: `Do not give out any of your contact information until you feel ready to take the dialogue further `
          + `or meet in real BlindDater.`
        }, {
          a: `If another member does not show a mutual interest, it is important not to let emotions take over and`
          + ` instead accept the situation.`,
          b: `Do not get angry or rude if you get rejected, that is unnecessary. Remember that there are many more `
          + `exciting members to familiarize yourself with.`
        }, {
          a: `If another member does not show a mutual interest, it is important to accept the situation and not let `
          + `your emotions take over. A good relationship is based on mutuality, a one-sided interest in a `
          + `relationship may give difficulties and could have been avoided and replaced with better options.`,
          b: `If you get rejected or badly treated, waste no energy on dull altercation that leads nowhere. Remember `
          + `that there are many other exciting members to familiarize yourself with.`
        }, {
          a: `If another member asks you for money or your bank details, that most likely means that the person has `
          + `no good intentions. We recommend that you then stop the contact with the person and report it to us.`,
          b: `Never give out any bank details. You will most likely be cheated out of your money.`
        }, {
          a: `People use BlindDater to talk and get to know each other. Therefore, you will probably encounter `
          + `opinions that do not match your own. Respect them! Do not start to engage in emotional arguments with `
          + `members you have not yet gotten to know.`,
          b: `Show respect for the opinions of others and avoid aggressive arguments. BlindDater is not the `
          + `right forum for this.`
        }, {
          a: `Always be yourself!How cliché it may sound, it is how you will gain the most when looking for
        friendship and trust.`,
          b: `Do not try to become or act like someone you are not.This always shines through, sooner rather than later.`
        }, {
          a: `Create a habit of telling a family member where you are going, who you meet and when you plan to go
        home from the date.Remember to always carry a fully charged cell phone.`,
          b: `Do not go on a date without telling a family member where you are going, who you meet, when you come
      home.Make also sure that you have battery left in your cell phone.`
        }
      ]
    },
  },
  se: {
    title: 'Vett & Etikett',
    main: {
      info: [
        `Är nätdejting ny mark för dig, så är det kanske inte helt självklart hur man ska gå till väga och vilket sätt `
        + `som egentligen är det bästa att ta kontakt med andra på.`,
        `Regler som är huggna i sten finns egentligen inte när det kommer till nätdejting. Däremot finns det vissa saker `
        + `som är viktiga att ta hänsyn till då det gäller att du vill att den du kontaktar ska bli intresserad av dig.`,
        `Idag träffar 25% av svenskar sin partner genom nätdejting, och vår målsättning är att bidra till flera långvariga `
        + `lyckträffar. Då kan tips och råd vara uppskattade, och vi vill därför bjuda på några erfarenheter vi noterat `
        + `från tidigare medlemmars feed-back:`,
      ],
    },
    options: {
      title: {
        a: 'Gör',
        b: 'Gör inte'
      },
      pairs: [
        {
          a: `Ta bara kontakt med de medlemmar som du faktiskt är intresserad av att lära känna efter att ha gått igenom `
          + `deras profiler. Det kommer i slutänden göra det enklare för alla, och du använder din tid rätt. På sikt `
          + `hjälper det även andra medlemmar, som presenterat sig otillräckligt, att ändra sitt förhållningssätt och `
          + `faktiskt bli uppmärksammade hos andra medlemmar.`,
          b: `”Spamma” inte flera medlemmar med samma standard-innehåll. Det kommer med största sannolikhet att leda `
          + `till irritation och att din chans till en bra början kan gå förlorad.`,
        }, {
          a: `Håll en schysst ton mot den du skriver med, och du kommer få så mycket mer tillbaka. Detsamma gäller det `
          + `bemötande du får, känns det inte okej är det såklart inget du ska tillåta.`,
          b: `Närma dig med takt och hänsyn och kom inte i närheten av att påminna om en skitstövel helt enkelt. Det är `
          + `varken något du kommer långt med eller något vi tillåter. `,
        }, {
          a: `Håll en schysst ton mot den du skriver med, och du kommer få så mycket mer tillbaka. Detsamma gäller det `
          + `bemötande du får, känns det inte okej är det såklart inget du ska tillåta.`,
          b: `Ge inte ut några av dina kontaktuppgifter förrän du känner dig redo att föra dialogen vidare eller träffas utanför`,
        }, {
          a: `Om en annan medlem inte visar ett ömsesidigt intresse är det viktigt att inte låta känslorna ta över och `
          + `acceptera situationen.`,
          b: `Bli inte arg eller otrevlig om du blir avvisad. Det är onödigt. Kom ihåg att det finns många fler `
          + `spännande medlemmar att bekanta sig med.`,
        }, {
          a: `Om en annan medlem inte visar ett ömsesidigt intresse är det viktigt att acceptera situationen och `
          + `inte låta känslorna ta över. En bra relation bygger på ömsesidighet, ett ensidigt intresse i en relation `
          + `kan komma att ge svårigheter som kunnat undvikas och ersättas med bättre scenarier. `,
          b: `Om du blir avvisad eller illa bemött, slösa ingen energi på tråkig ordväxling som inte leder någonstans. `
          + `Kom ihåg att det finns många andra och spännande medlemmar att bekanta sig med.`,
        }, {
          a: `Om en annan medlem ber dig om pengar eller dina bankuppgifter, så har personen med största sannolikhet `
          + `inga goda avsikter. Vi rekommenderar att du då avbryter kontakten och rapporterar det till oss.`,
          b: `Lämna aldrig ut några bankuppgifter. Du kommer med största sannolikhet bli lurad på pengar.`,
        }, {
          a: `Människor använder BlindDater för att prata och lära känna varandra. Därför kommer du förmodligen `
          + `stöta på åsikter som inte stämmer överens med dina. Respektera dem! Gå inte in i utbyte av känslofylld `
          + `argumentation med medlemmar du inte hunnit lära känna. `,
          b: `Visa respekt för andras åsikter och inled ingen aggressiv argumentation. BlindDater är inte rätt forum för detta.`,
        }, {
          a: `Var alltid dig själv! Hur klyschigt det än må låta, så är det vad du kommer att vinna på när du söker vänskap och tillit.`,
          b: `Försök inte vara eller agera någon du inte är. Detta skiner alltid igenom, snarare förr än senare.`,
        }, {
          a: `Ta för vana att tala om för en anhörig vart du ska, vem du träffar och när du planerar att gå hem från `
          + `en dejt. Kom ihåg att alltid bära en fulladdad mobil.`,
          b: `Gå inte på dejt utan att berätta för någon anhörig vart du ska, vem du träffar, när du ska komma hem, `
          + `och med låg batterinivå i din mobiltelefon.`,
        }
      ]
    },
  },
};

/**
 * @function getPageIndexStatic
 * @description get exemplar of configs
 * @param {IMainConfig.LangField} lang
 * @return {IUserStatic.Info}
 */
export function getPageIndexStatic(lang: IMainConfig.LangField): IIndexStatic.Info {
  if (_pageIndexStatic[lang] === void 0) {
    return _pageIndexStatic.en;
  }

  return _pageIndexStatic[lang];
}
