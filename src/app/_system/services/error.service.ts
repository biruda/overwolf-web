'use strict';


export namespace IErrorHandler {

  export namespace Data {

    export type ErrorType = 'critical' | 'error' | 'warning';
  }

  export namespace Catch {

    export interface Opts {
      componentName: string;
      methodName: string;
      methodParams: Array<string | number | object>;
      errorMessage: string;
      errorType: string;
      path: string;
      stack: string;
    }
  }
}

/**
 * @class ErrorBase
 * @extends Error
 */
class ErrorBase extends Error {
  public message: string;
  public info: string;
  private project = 'WEB';

  /**
   * @constructor
   * @param {string} name
   * @param {number} type
   * @param {Array<string | number | Object>} params
   */
  constructor(name: string, type: IErrorHandler.Data.ErrorType, params: Array<string | number | object>) {
    super(params[0].toString());

    params.splice(0, 1);
    this.info = params[0] !== void 0 ? params[0].toString() : null;
    const {componentName, methodName, path} = errorTrace(this.stack, 3);

    catchError({
      componentName: this.project + '_' + name + '_' + componentName,
      methodName: methodName,
      methodParams: params,
      errorMessage: this.message,
      errorType: type,
      path: path,
      stack: this.stack
    });
  }
}

/**
 * @class ApiError
 * @extends ErrorBase
 */
export class ApiError extends ErrorBase {
  public message: string;
  public info: string;

  /**
   * @constructor
   * @param {Array<string | number | Object>} params
   */
  constructor(params: Array<string | number | object>) {
    super('ApiError', 'critical', params);
  }
}

/**
 * @class ClientError
 * @extends ErrorBase
 */
export class ClientError extends ErrorBase {
  public message: string;
  public info: string;

  /**
   * @constructor
   * @param {Array<string | number | Object>} params
   */
  constructor(params: Array<string | number | object>) {
    super('ClientError', 'critical', params);
  }
}

/**
 * @function errorTrace
 * @description prepare error
 * @param {string} stack
 * @param {number} level
 * @return {object} trace
 * @return {string} trace.moduleName
 * @return {string} trace.methodName
 * @return {string} trace.path
 */
function errorTrace(stack: string, level: 2 | 3): { componentName: string, methodName: string, path: string } {
  let componentName, methodName, path;
  try {
    const methodStack = stack.split('\n')[level];
    const parentInfo = methodStack
      .split('(')[0]
      .replace('at', '')
      .replace(/ /g, '')
      .split('.');

    componentName = parentInfo[parentInfo.length - 2];
    methodName = parentInfo[parentInfo.length - 1];
    path = methodStack.substring(methodStack.indexOf('(') + 1, methodStack.indexOf(')'));
  } catch (e) {
  }

  return {componentName, methodName, path};
}

/**
 * @function catchError
 * @description handle the error
 */
function catchError(opts: IErrorHandler.Catch.Opts): void {
  // if (process.env['NODE_ENV'] !== 'development') {
  //   // init notification
  //   _Internal._monitoringClient.monitorError({
  //     module: moduleName,
  //     method: methodName,
  //     params: JSON.stringify(methodParams),
  //     message: errorMessage,
  //     type: errorType,
  //   });
  // }

  // save to log
  const logData = `
  **********************************************************************************
  component:  ${opts.componentName};
  method:     ${opts.methodName};
  params:     ${JSON.stringify(opts.methodParams)};
  message:    ${opts.errorMessage};
  date:       ${new Date()};
  path:       ${opts.path};
  type:       ${opts.errorType};
  stack:      ${opts.stack};
  **********************************************************************************` + '\n';


  console.error(logData);
}
