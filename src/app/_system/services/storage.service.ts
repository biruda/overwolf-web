'use strict';
// services
import {CryptoService} from './crypto.service';


export namespace IStorageService {

  export type Driver = 'session' | 'local';

  export type Key = 'adminToken' | 'adminProfile';
}

const isEncoded = true;

export class StorageService {

  static get(driver: IStorageService.Driver, key: IStorageService.Key): string | null {
    const keyEncoded = isEncoded === true ? CryptoService.hash(key) : key;

    let data: string | null = null;
    if (driver === 'session') {
      data = sessionStorage.getItem(keyEncoded);
    } else if (driver === 'local') {
      data = localStorage.getItem(keyEncoded);
    }

    return data !== null
      ? (
        isEncoded === true
          ? CryptoService.decrypt(data)
          : data
      )
      : null;
  }

  static set(driver: IStorageService.Driver, key: IStorageService.Key, data: string) {
    const keyEncoded = isEncoded === true ? CryptoService.hash(key) : key;
    const dataEncoded = isEncoded === true ? CryptoService.encrypt(data) : data;

    if (driver === 'session') {
      sessionStorage.setItem(keyEncoded, dataEncoded);
    } else if (driver === 'local') {
      localStorage.setItem(keyEncoded, dataEncoded);
    }
  }

  static remove(driver: IStorageService.Driver, key: IStorageService.Key) {
    const keyEncoded = isEncoded === true ? CryptoService.hash(key) : key;

    if (driver === 'session') {
      sessionStorage.removeItem(keyEncoded);
    } else if (driver === 'local') {
      localStorage.removeItem(keyEncoded);
    }
  }

  static clear(driver: IStorageService.Driver) {
    if (driver === 'session') {
      sessionStorage.clear();
    } else if (driver === 'local') {
      localStorage.clear();
    }
  }
}
