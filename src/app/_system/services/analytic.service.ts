'use strict';
// modules
import {Subscription} from 'rxjs/Subscription';
// configs
import {getAnalyticConfig, IAnalyticConfig} from '../configs/analytic.configs';


export namespace IAnalyticService {

  export namespace Storage {

    export interface Internal {
      configs: Configs;
      watchers: Watchers;
    }

    interface Configs {
      _analyticConfig: IAnalyticConfig.Config;
      user: IAnalyticConfig.External.MixpanelParams;
      isStarted: boolean;
    }

    interface Watchers {
      logout: Subscription;
    }
  }

  export namespace Send {

    export interface Event {
      page?: string;
      session?: string;
      action?: string;
    }
  }
}

const _Internal: IAnalyticService.Storage.Internal = {
  configs: {
    _analyticConfig: getAnalyticConfig(),
    isStarted: false,
    user: {
      $email: 'email',
      $first_name: 'name',
      $last_name: 'lname',
      $created: 'created_at',
      $last_login: new Date(),
      gender: 'gender',
      age: 1,
      appCountry: 'country_code',
    },
  },
  watchers: {
    logout: null,
  },
};

export class AnalyticService {

  /**
   * @function startMixpanel
   * @description init mixpanel
   * @private
   */
  private startMixpanel() {
    // check is started
    if (_Internal.configs.isStarted === true) {
      return;
    }
    _Internal.configs.isStarted = true;

    // send init event
    this.sendMixpanel({session: _Internal.configs._analyticConfig.events.session.start});
  }

  /**
   * @function sendExternal
   * @description init tracking at analytic' system
   * @param {number} driver
   * @param {IAnalyticService.Send.Event} event
   */
  sendExternal(driver: number, event: IAnalyticService.Send.Event) {
    if (driver === _Internal.configs._analyticConfig.drivers.api) {
    } else if (driver === _Internal.configs._analyticConfig.drivers.google) {
    } else if (driver === _Internal.configs._analyticConfig.drivers.mixpanel) {
      this.sendMixpanel(event);
    }
  }

  /**
   * @function sendMixpanel
   * @description
   * @param {IAnalyticService.Send.Event} opts
   * @private
   */
  private sendMixpanel(opts: IAnalyticService.Send.Event) {
    this.startMixpanel();

    let event = '';
    if (opts.session !== void 0) {
      event = _Internal.configs._analyticConfig.events.groups.session + '/' + opts.session;
    } else if (opts.page !== void 0) {
      event = _Internal.configs._analyticConfig.events.groups.page + opts.page;
    } else if (opts.action !== void 0) {
      event = _Internal.configs._analyticConfig.events.groups.action + '/' + opts.action;
    }

    if (_Internal.configs.user !== null) {
      // mixpanel.track(event, this.configs.admin.options);
    } else {
      // mixpanel.track(event);
    }
  }
}
