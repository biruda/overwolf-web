'use strict';
// modules
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subscription} from 'rxjs/Subscription';
// models
import {User} from '../models/users/User';
// configs
import {IMainConfig} from '../configs/main.config';
import {IApiConfig} from '../configs/api.config';
// static
import {getErrors} from '../../_static/errors';

namespace IApi {

  export interface Response {
    err: boolean;
    msg: string ;
    data: object[];
  }

  export interface OptsCb {
    (err: null | string | object | object[], data: object[] | boolean[] | string[]);
  }

  export interface ResponseResult {
    err: string | object | object[];
    data: object[];
  }

  export interface Internal {
    authQueue: Function[];
    unSecure: boolean;
    language: IMainConfig.LangField;
    isLogout: boolean;
    watchers: {
      lang: Subscription,
    };
  }
}

const _Internal: IApi.Internal = {
  authQueue: [],
  unSecure: null,
  language: 'en',
  isLogout: null,
  watchers: {
    lang: null,
  },
};

function parseResponse(res: string | IApi.Response, status?: number): IApi.ResponseResult {
  const result: IApi.ResponseResult = {
    err: null,
    data: null,
  };
  const response: IApi.Response = typeof res === 'string' ? JSON.parse(res) : res;
  const _msg = response.msg;
  const _data = response.data;
  const _error = response.err;

  // get errors
  if (_error === true) {
    // result.err = _data !== void 0 && _data !== '' ? _data : _msg;
    result.err = _data ? _data : _msg;
  }

  // get data
  if (_error === false) {
    result.data = _data !== void 0
      ? (Array.isArray(_data) === true ? _data : [_data])
      : [{_msg}];
  }

  // check for unAuth
  if (status === 401) {
    result.err = getErrors(_Internal.language).notSecure;
    logout();
  }
  if (result.err === 'wrong token') {
    result.err = getErrors(_Internal.language).notSecure;
    logout();
  }

  // check for removed
  if (status === 403) {
    result.err = getErrors(_Internal.language).isRemoved;
    logout();
  }
  if (result.err === 'isRemoved') {
    result.err = getErrors(_Internal.language).isRemoved;
    logout();
  }

  return result;
}

function logout() {
  if (_Internal.isLogout === true) {
    return;
  }

  _Internal.isLogout = true;
  setTimeout(() => {
    User.singOut();
    _Internal.isLogout = false;
  }, 500);
}

@Injectable()
export class ApiService {
  private headers: { [k: string]: string } = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  };

  constructor(private _http: HttpClient) {
  }

  get(model: IApiConfig.Item, query: string, cb: IApi.OptsCb): void {
    // configure request' params
    let url = model.domain + model.path;
    if (query !== '') {
      url += '?' + query;
    }
    if (url.indexOf('lang=') === -1) {
      url += (url.indexOf('?') !== -1 ? '&' : '?') + `lang=${_Internal.language}`;
    }

    // update headers if needed
    const _headers = Object.assign({}, this.headers);
    const token = User.getToken();
    if (token !== null) {
      _headers['Authorization'] = token;
    } else {
      // wait before authentication will be executed
      _Internal.authQueue.push((callback) => {
        this.get(model, query, (funcErr: null | object[], funcData: object[]) => {
          cb(funcErr, funcData);
          callback();
        });
      });
      return;
    }

    this._http.get(url, {
      headers: new HttpHeaders(_headers)
    })
      .subscribe(
        (res: IApi.Response) => {
          const result: IApi.ResponseResult = parseResponse(res);
          cb(result.err, result.data);
        },
        (err) => {
          const result: IApi.ResponseResult = parseResponse(err.error, err.status);
          cb(result.err, result.data);
        }
      );
  }

  post(model: IApiConfig.Item, query: string, data: object, cb: IApi.OptsCb, isAuthRequest?: true): void {
    // configure request' params
    let url = model.domain + model.path;
    if (query !== '') {
      url += '?' + query;
    }
    if (url.indexOf('lang=') === -1) {
      url += (url.indexOf('?') !== -1 ? '&' : '?') + `lang=${_Internal.language}`;
    }

    // update headers if needed
    const _headers = Object.assign({}, this.headers);
    const token = User.getToken();
    if (token !== null) {
      _headers['Authorization'] = token;
    } else if (isAuthRequest !== true) {
      // wait before authentication will be executed
      _Internal.authQueue.push((callback) => {
        this.post(model, query, data, (funcErr: null | object[], funcData: object[]) => {
          cb(funcErr, funcData);
          callback();
        });
      });
      return;
    }

    // init request
    this._http.post(url, data, {
      headers: new HttpHeaders(_headers)
    })
      .subscribe(
        (res: IApi.Response) => {
          // init auth' queue execution
          if (isAuthRequest === true) {
            setTimeout(() => this.queueTick(), 100);
          }

          const result: IApi.ResponseResult = parseResponse(res);
          cb(result.err, result.data);
        },
        (err) => {
          const result: IApi.ResponseResult = parseResponse(err.error, err.status);
          cb(result.err, result.data);
        }
      );
  }

  put(model: IApiConfig.Item, query: string, data: object, cb: IApi.OptsCb, isAuthRequest?: true): void {
    // configure request' params
    let url = model.domain + model.path;
    if (query !== '') {
      url += '?' + query;
    }
    if (url.indexOf('lang=') === -1) {
      url += (url.indexOf('?') !== -1 ? '&' : '?') + `lang=${_Internal.language}`;
    }

    // update headers if needed
    const _headers = Object.assign({}, this.headers);
    const token = User.getToken();
    if (token !== null) {
      _headers['Authorization'] = token;
    } else if (isAuthRequest !== true) {
      // wait before authentication will be executed
      _Internal.authQueue.push((callback) => {
        this.put(model, query, data, (funcErr: null | object[], funcData: object[]) => {
          cb(funcErr, funcData);
          callback();
        });
      });
      return;
    }

    // init request
    this._http.put(url, data, {
      headers: new HttpHeaders(_headers)
    })
      .subscribe(
        (res: IApi.Response) => {
          // init auth' queue execution
          if (isAuthRequest === true) {
            setTimeout(() => this.queueTick(), 100);
          }

          const result: IApi.ResponseResult = parseResponse(res);
          cb(result.err, result.data);
        },
        (err) => {
          const result: IApi.ResponseResult = parseResponse(err.error, err.status);
          cb(result.err, result.data);
        }
      );
  }

  remove(model: IApiConfig.Item, query: string, cb: IApi.OptsCb): void {
    // configure request' params
    let url = model.domain + model.path;
    if (query !== '') {
      url += '?' + query;
    }
    if (url.indexOf('lang=') === -1) {
      url += (url.indexOf('?') !== -1 ? '&' : '?') + `lang=${_Internal.language}`;
    }

    // update headers if needed
    const _headers = Object.assign({}, this.headers);
    const token = User.getToken();
    if (token !== null) {
      _headers['Authorization'] = token;
    } else {
      // wait before authentication will be executed
      _Internal.authQueue.push((callback) => {
        this.remove(model, query, (funcErr: null | object[], funcData: object[]) => {
          cb(funcErr, funcData);
          callback();
        });
      });
      return;
    }

    this._http.delete(url, {
      headers: new HttpHeaders(_headers)
    })
      .subscribe(
        (res: IApi.Response) => {
          const result: IApi.ResponseResult = parseResponse(res);
          cb(result.err, result.data);
        },
        (err) => {
          const result: IApi.ResponseResult = parseResponse(err.error, err.status);
          cb(result.err, result.data);
        }
      );
  }

  private queueTick() {
    // check if queue exist
    if (_Internal.authQueue.length === 0) {
      return;
    }

    // get current function
    const f = _Internal.authQueue[0];
    _Internal.authQueue.splice(0, 1);

    // execute current function
    f(() => {
      // get next function if exist
      if (_Internal.authQueue.length > 0) {
        this.queueTick();
      }
    });
  }
}
