'use strict';
// modules
import {AES, enc, MD5} from 'crypto-js';
// configs
import {getMainConfig, IMainConfig} from '../configs/main.config';


export namespace ICryptoService {

  export namespace Storage {

    export interface Internal {
      configs: Configs;
    }

    interface Configs {
      _mainConfig: IMainConfig.Config;
    }
  }
}

/**
 * @const _Internal
 * @type {ICryptoService.Storage.Internal}
 * @private
 */
const _Internal: ICryptoService.Storage.Internal = {
  configs: {
    _mainConfig: getMainConfig(),
  }
};

/**
 * @class CryptoService
 */
export class CryptoService {

  /**
   * @function hash
   * @param {string} message
   * @return {string}
   * @static
   */
  static hash(message: string): string {
    const cipherText = MD5(message, _Internal.configs._mainConfig.auth.cache);
    return cipherText.toString();
  }

  /**
   * @function encrypt
   * @param {string} message
   * @return {string}
   * @static
   */
  static encrypt(message: string): string {
    const cipherText = AES.encrypt(message, _Internal.configs._mainConfig.auth.cache);
    return cipherText.toString();
  }

  /**
   * @function decrypt
   * @param {string} cipherText
   * @return {string}
   * @static
   */
  static decrypt(cipherText: string): string {
    const message = AES.decrypt(cipherText, _Internal.configs._mainConfig.auth.cache);
    return message.toString(enc.Utf8);
  }
}
