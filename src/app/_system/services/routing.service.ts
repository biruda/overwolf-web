'use strict';
// models
import {Subject} from 'rxjs/Subject';


export namespace IRoutingService {

  export namespace Storage {

    export interface Internal {
      updates: Updates;
    }

    export interface Updates {
      appRouter: Subject<string>;
    }
  }
}

const _Internal: IRoutingService.Storage.Internal = {
  updates: {
    appRouter: new Subject<string>(),
  },
};

export class RoutingService {

  /**
   * @function setAppRender
   * @description init change route after restriction' check
   * @param {string} route
   * @private
   */
  static setAppRender(route: string) {

    // todo: implement check restriction of admin to current page

    _Internal.updates.appRouter.next('/' + route);
  }

  /**
   * @function getAppSubscription
   * @description return watcher to apps' routing
   * @return {Subject<string>}
   */
  static getAppSubscription(): Subject<string> {
    return _Internal.updates.appRouter;
  }
}
