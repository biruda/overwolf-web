'use strict';
// modules
import {Subject} from 'rxjs/Subject';


export namespace IModal {

  export namespace Notification {

    export type NoteType = 'primary' | 'success' | 'info' | 'warning' | 'danger';

    export interface Note {
      type?: IModal.Notification.NoteType;
      options?: {};
      content: {
        title?: string,
        info: any
      }[];
    }
  }

  export interface Internal {
    $notification: Subject<IModal.Notification.Note>;
  }
}

const _Internal: IModal.Internal = {
  $notification: new Subject<IModal.Notification.Note>(),
};

export class NotificationService {

  static getSubscription(): Subject<IModal.Notification.Note> {
    return _Internal.$notification;
  }

  static sendUpdates(note: IModal.Notification.Note) {
    _Internal.$notification.next(note);
  }
}
