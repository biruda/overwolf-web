'use strict';

export namespace IMainConfig {

  export interface LangContent {
    en: any;
    se: any;
  }

  export type LangField = 'en' | 'se';

  export const LangList: LangField[] = ['en', 'se'];

  export interface Config {
    auth: {
      cache: string;
    };
  }
}

const _mainConfig: IMainConfig.Config = {
  auth: {
    cache: 'NMNC*(SDS(i0())((*&T^6oN*( 89 a r3289 90nicdsnsncksu^G&^H*hds78',
  },
};

export function getMainConfig(): IMainConfig.Config {
  return _mainConfig;
}
