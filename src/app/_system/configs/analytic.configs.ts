'use strict';
// configs
import {getStaticConfig, IStatic} from './_static.config';


// included config
const _staticConfig: IStatic.Config = getStaticConfig();

// const urls

/**
 * @namespace IAnalyticConfig
 */
export namespace IAnalyticConfig {

  /**
   * @namespace External
   */
  export namespace External {

    /**
     * @interface Mixpanel
     */
    export interface Mixpanel {
      init: (appID: string) => void;
      identify: (userID: string) => void;
      people: {
        set: (params: MixpanelParams) => void;
      };
      track: (event: string, params?: MixpanelEventParams) => void;
    }

    /**
     * @interface MixpanelParams
     */
    export interface MixpanelParams {
      $email: string;
      $first_name: string;
      $last_name: string;
      $created: Date | string;
      $last_login: Date;
      gender: string;
      age: number;
      appCountry: string;
    }

    /**
     * @type MixpanelEventParams
     */
    export type MixpanelEventParams = MixpanelParams;

    /**
     * @interface Google
     */
    export interface Google {

    }
  }

  /**
   * @interface Config
   */
  export interface Config {
    events: ConfigEvents;
    drivers: ConfigDrivers;
    external: ConfigExternal;
  }

  /**
   * @interface ConfigExternal
   * @private
   */
  interface ConfigExternal {
    mixpanel: {
      dev: string;
      prod: string;
    };
  }

  /**
   * @interface ConfigEvents
   * @private
   */
  interface ConfigEvents {
    groups: {
      page: string,
      session: string,
      action: string,
    };
    session: {
      start: string,
      logout: string,
    };
    members: {
      reportAdd: string,
      blockAdd: string,
      blockRemove: string,
      favoriteAdd: string,
      favoriteRemove: string,
      likeAdd: string,
      likeRemove: string,
      profileVisit: string,
      searchLoadMore: string
    };
    messages: {
      messageSendNormal: string,
      messageSendMatch: string,
      messageSendHour: string,
      flirtSend: string,
    };
    hour: {
      guestLoad: string,
    };
    photos: {
      coverUpload: string,
      albumUpload: string,
      avatarUpload: string,
    };
    test: {
      pass: string
    };
  }

  /**
   * @interface ConfigDrivers
   * @private
   */
  interface ConfigDrivers {
    api: 0;
    mixpanel: 1;
    google: 2;
  }
}

/**
 * @const _analyticConfig
 * @description internal release from client side
 * @type {IAnalyticConfig.Config}
 * @private
 */
const _analyticConfig: IAnalyticConfig.Config = {
  events: {
    groups: {
      page: 'page',
      session: 'session',
      action: 'action',
    },
    session: {
      start: 'start',
      logout: 'logout',
    },
    members: {
      reportAdd: 'members/report/add',
      blockAdd: 'members/block/add',
      blockRemove: 'members/block/remove',
      favoriteAdd: 'members/favorite/add',
      favoriteRemove: 'members/favorite/remove',
      likeAdd: 'members/like/add',
      likeRemove: 'members/like/remove',
      profileVisit: 'members/profile/visit',
      searchLoadMore: 'members/search/load'
    },
    messages: {
      messageSendNormal: 'message/send/normal',
      messageSendMatch: 'message/send/match',
      messageSendHour: 'message/send/hour',
      flirtSend: 'message/flirt/send',
    },
    hour: {
      guestLoad: 'hour/guest/load',
    },
    photos: {
      coverUpload: 'photos/cover/upload',
      albumUpload: 'photos/album/upload',
      avatarUpload: 'photos/avatar/upload',
    },
    test: {
      pass: 'test/pass'
    },
  },
  drivers: {
    api: 0,
    mixpanel: 1,
    google: 2,
  },
  external: {
    mixpanel: {
      dev: 'c18de68530cb99bcde3a07a1ea45ec15',
      prod: '4a53ae52eec139960a891f78f9be7e1b',
    },
  },
};

/**
 * @function getAnalyticConfig
 * @description get config' exemplar
 * @return {IAnalyticConfig.Config}
 */
export function getAnalyticConfig(): IAnalyticConfig.Config {
  return _analyticConfig;
}
