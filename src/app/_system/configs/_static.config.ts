'use strict';

export namespace IStatic {

  export interface Domains {
    current: string;
  }

  export interface Urls {
    index: string;
    usersGroup: {
      sign: string;
    };
    appsGroup: {
      appList: string;
    };
    releaseGroup: {
      note: string;
    };
  }

  export interface Config {
    domains: IStatic.Domains;
    urls: IStatic.Urls;
  }

}

export function getApiDomain(): string {
  return window.location.hostname === 'localhost' ? 'http://127.0.0.1' : 'https://api.overwolf.com';
}

const _staticConfig: IStatic.Config = {
  domains: {
    current: getApiDomain(),
  },
  urls: {
    index: '',
    usersGroup: {
      sign: 'users/auth',
    },
    appsGroup: {
      appList: 'apps/app',
    },
    releaseGroup: {
      note: 'release/note',
    },
  },
};

export function getStaticConfig(): IStatic.Config {
  return _staticConfig;
}
