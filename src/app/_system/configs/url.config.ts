'use strict';
// static config
import {getStaticConfig, IStatic} from './_static.config';


// included config
const _staticConfig: IStatic.Config = getStaticConfig();

export namespace IUrlConfig {

  export interface Config {
    usersGroup: {
      sign: ConfigItem;
    };
    releaseGroup: {
      note: ConfigItem;
    };
    appGroup: {
      appList: ConfigItem;
    };
  }

  export interface ConfigItem {
    url: string;
  }
}

const _urlConfig: IUrlConfig.Config = {
  usersGroup: {
    sign: {
      url: _staticConfig.urls.usersGroup.sign,
    },
  },
  releaseGroup: {
    note: {
      url: _staticConfig.urls.releaseGroup.note,
    },
  },
  appGroup: {
    appList: {
      url: _staticConfig.urls.appsGroup.appList,
    },
  }
};

export function getUrlConfig(): IUrlConfig.Config {
  return _urlConfig;
}
