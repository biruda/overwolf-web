'use strict';
// static config
import {getStaticConfig, IStatic} from './_static.config';


const _staticConfig: IStatic.Config = getStaticConfig();

const _domains = {
  api: _staticConfig.domains.current,
};

export namespace IApiConfig {

  export interface Item {
    domain: string;
    path: string;
  }

  export interface Models {
    apps: {
      app: IApiConfig.Item,
    };
    release: {
      note: IApiConfig.Item,
    };
    users: {
      authGoogle: IApiConfig.Item,
    };
  }

  export interface Config {
    api: string;
    models: IApiConfig.Models;
  }

}

const _apiConfig: IApiConfig.Config = {
  api: _domains.api,
  models: {
    apps: {
      app: {
        domain: _domains.api,
        path: `/v1/apps/app`,
      },
    },
    release: {
      note: {
        domain: _domains.api,
        path: `/v1/release/note`,
      },
    },
    users: {
      authGoogle: {
        domain: _domains.api,
        path: `/v1/user/auth/google`,
      },
    },
  }
};

export function getApiConfig(): IApiConfig.Config {
  return _apiConfig;
}
