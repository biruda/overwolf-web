'use strict';

/**
 * @namespace INotifyConfig
 */
export namespace INotifyConfig {

  /**
   * @interface Config
   */
  export interface Config {
    types: {
      primary: 1,
      success: 2,
      info: 3,
      warning: 4,
      danger: 5
    };
    options: {
      time: number
    };
  }
}

/**
 * @const _notifyConfig
 * @description configs for components of Notification group
 * @type {INotifyConfig.Config}
 * @private
 */
const _notifyConfig: INotifyConfig.Config = {
  types: {
    primary: 1,
    success: 2,
    info: 3,
    warning: 4,
    danger: 5
  },
  options: {
    time: 3000
  }
};

/**
 * @function getNotifyConfig
 * @description return config' exemplar
 * @return {INotifyConfig.Config}
 */
export function getNotifyConfig(): INotifyConfig.Config {
  return _notifyConfig;
}
