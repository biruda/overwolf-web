'use strict';
// configs
import {_macros, getErrorsFull, IErrors} from '../../_static/errors';


/**
 * @namespace IValidationConfig
 */
export namespace IValidationConfig {

  /**
   * @interface Config
   */
  export interface Config {
    _macros: string;
    regex: {
      mail: RegExp,
      username: RegExp,
      zip: {
        SE: RegExp,
        US: RegExp,
      },
    };
    limits: {
      username: {
        from: number,
        to: number,
      },
      password: {
        from: number,
        to: number,
      },
    };
    message: IErrors.Configs;
  }
}

/**
 * @const validationConfig
 * @description regexp, limits and error' messages
 * @private
 */
const _validationConfig: IValidationConfig.Config = {
  _macros: _macros,
  regex: {
    mail: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    username: /^[a-zA-Z0-9_\-]*$/,
    zip: {
      US: /\d{5}([ \-]\d{4})?/,
      SE: /\d{3}[ ]?\d{2}/,
    },
  },
  limits: {
    username: {
      from: 3,
      to: 15,
    },
    password: {
      from: 8,
      to: 20,
    },
  },
  message: getErrorsFull()
};

/**
 * @function getValidationConfig
 * @description get config' exemplar
 * @return {IValidationConfig.Config}
 */
export function getValidationConfig(): IValidationConfig.Config {
  return _validationConfig;
}
