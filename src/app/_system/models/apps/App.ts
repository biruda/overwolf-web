'use strict';
// modules
import {Subject} from 'rxjs/Subject';
// services
import {ApiError} from '../../services/error.service';
// configs
import {getApiConfig, IApiConfig} from '../../configs/api.config';


export namespace IApp {

  export namespace Api {

    export type ObjectID = string;

    export type Response = IApp.Api.Item[];

    export interface Item {
      _id: ObjectID;
      AppName: string;
      Author: ObjectID;
      created_at: Date;
    }

    export interface Request {
      AppName: string;
    }
  }

  export namespace Options {

    export interface NewAppModes {
      AppName: any;
    }

    export type FieldType =
      'AppName'
      | 'Author'
      | 'created_at';

    export const $FieldList: FieldType[] = [
      'AppName', 'Author', 'created_at'
    ];

    export interface FieldNames {
      AppName: string;
      Author: string;
      created_at: string;
    }
  }

  export namespace Data {

    export interface WatcherApiUpdates {
      model: IApiConfig.Item;
      updates: Subject<IApp.Data.WatcherApi>;
    }

    export interface WatcherApi {
      query: string;
      body: IApp.Api.Request;
      cb: WatcherApiCb;
    }

    export type WatcherApiCb = (err: null | string, data: IApp.Api.Response) => void;
  }

  export namespace Storage {

    export interface Internal {
      data: Data;
      configs: Configs;
      updates: Updates;
    }

    interface Data {
      activeApp: string;
      appList: IApp.Api.Item[];
    }

    interface Configs {
      _model: IApiConfig.Item;
    }

    interface Updates {
      api: Subject<IApp.Data.WatcherApi>;
    }
  }
}

const _Internal: IApp.Storage.Internal = {
  data: {
    activeApp: null,
    appList: [],
  },
  configs: {
    _model: getApiConfig().models.apps.app,
  },
  updates: {
    api: new Subject<IApp.Data.WatcherApi>(),
  },
};

export class App {

  static get(): IApp.Api.Item[] {
    return _Internal.data.appList;
  }

  static setActiveApp(appID: string): void {
    _Internal.data.activeApp = appID;
  }

  static getActiveApp(): string {
    return _Internal.data.activeApp;
  }

  static send(item: IApp.Api.Request) {
    AppUtils.initApi(item, (err: null | string) => {
    });
  }

  static getSubscriptionApi(): IApp.Data.WatcherApiUpdates {
    AppUtils.initialisation();
    return {
      model: _Internal.configs._model,
      updates: _Internal.updates.api,
    };
  }
}

class AppUtils {

  static initialisation() {
    // wait for getting api updates
    setTimeout(() => AppUtils.load(), 10);
  }

  static load(): void {
    AppUtils.initApi(null, (err: null | string, data: IApp.Api.Response) => {
      if (err !== null) {
        return new ApiError([err]);
      }

      _Internal.data.appList = data;
    });
  }

  static initApi(body: null | IApp.Api.Request, cb: IApp.Data.WatcherApiCb) {
    _Internal.updates.api.next({
      query: '',
      body,
      cb: (err: null | string, data: IApp.Api.Response) => cb(err, data)
    });
  }

  static clear() {
    _Internal.data.activeApp = null;
    _Internal.data.appList = [];
  }
}
