'use strict';
// modules
import {Subject} from 'rxjs/Subject';
// services
import {StorageService} from '../../services/storage.service';
// configs
import {getApiConfig, IApiConfig} from '../../configs/api.config';


export namespace IUser {

  export namespace Api {

    export interface AuthRequest {
      googleToken: string;
    }

    export interface AuthResponse {
      token: string;
      name: null | string;
    }
  }

  export namespace Info {

    export interface User {
      name: string;
      token: string;
    }

    export type DataSignIn = true;

    export type DataSignOut = true;
  }

  export namespace Storage {

    export interface Internal {
      data: Data;
      configs: Configs;
      // watchers: Watchers;
      updates: Updates;
    }

    export interface Data {
      admin: IUser.Info.User;
      hashParams: null | object;
    }

    export interface Updates {
      main: Subject<IUser.Info.User>;
      singIn: Subject<IUser.Info.DataSignIn>;
      singOut: Subject<IUser.Info.DataSignOut>;
    }

    export interface Configs {
      _apiConfig: IApiConfig.Config;
      isInit: boolean;
    }
  }
}

const _Internal: IUser.Storage.Internal = {
  data: {
    admin: null,
    hashParams: null,
  },
  configs: {
    _apiConfig: getApiConfig(),
    isInit: false,
  },
  // watchers: {},
  updates: {
    main: new Subject<IUser.Info.User>(),
    singIn: new Subject<IUser.Info.DataSignIn>(),
    singOut: new Subject<IUser.Info.DataSignOut>(),
  },
};

export class User {
  public model: IApiConfig.Item = _Internal.configs._apiConfig.models.users.authGoogle;

  static get(): IUser.Info.User {
    return _Internal.data.admin;
  }

  static getSubscriptionSignIn(): Subject<IUser.Info.DataSignIn> {
    return _Internal.updates.singIn;
  }

  static getSubscriptionSignOut(): Subject<IUser.Info.DataSignOut> {
    return _Internal.updates.singOut;
  }

  static getToken(): string {
    return _Internal.data.admin !== null ? _Internal.data.admin.token : null;
  }

  static getHashParams(): object {
    return _Internal.data.hashParams;
  }

  static set(admin: IUser.Info.User) {
    // if (admin.token === void 0 || admin.rights === void 0 || admin.status === void 0) {
    if (admin.token === void 0) {
      return;
    }

    _Internal.data.admin = admin;

    // UserUtils.saveToken(admin.token);
    UserUtils.saveProfile(admin);
    UserUtils.sendUpdates();
    UserUtils.sendUpdatesSignIn();
  }

  static setHashParams(data: object) {
    _Internal.data.hashParams = data;
  }

  static singOut() {
    UserUtils.clear();
    UserUtils.cleanToken();
    UserUtils.cleanProfile();
    UserUtils.sendUpdatesSignOut();
  }
}

class UserUtils {

  static sendUpdates() {
    const data: IUser.Info.User = _Internal.data.admin;
    _Internal.updates.main.next(data);
  }

  static sendUpdatesSignIn() {
    const data: IUser.Info.DataSignIn = true;
    _Internal.updates.singIn.next(data);
  }

  static sendUpdatesSignOut() {
    const data: IUser.Info.DataSignOut = true;
    _Internal.updates.singOut.next(data);
  }

  static loadToken() {
    const token = StorageService.get('local', 'adminToken');
    if (token !== null) {
      _Internal.data.admin = {
        name: null,
        token: token,
      };
    }
  }

  static loadProfile() {
    const profileStr = StorageService.get('local', 'adminProfile');
    if (profileStr === null) {
      return;
    }

    let admin: IUser.Info.User;
    try {
      admin = JSON.parse(profileStr);
    } catch (e) {
      return;
    }

    // if (admin === void 0 || admin.token === void 0 || admin.rights === void 0 || admin.status === void 0) {
    if (admin === void 0 || admin.token === void 0) {
      return;
    }
    _Internal.data.admin = admin;
  }

  static saveProfile(admin: IUser.Info.User) {
    StorageService.set('local', 'adminProfile', JSON.stringify(admin));
  }

  static saveToken(token: string) {
    StorageService.set('local', 'adminToken', token);
  }

  static cleanToken() {
    StorageService.remove('local', 'adminToken');
  }

  static cleanProfile() {
    StorageService.remove('local', 'adminProfile');
  }

  static clear() {
    _Internal.data.admin = null;
  }
}

// get token from cache | load profile for session
if (_Internal.configs.isInit === false) {
  _Internal.configs.isInit = true;
  UserUtils.loadProfile();
}
