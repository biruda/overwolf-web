'use strict';
// modules
import {Subject} from 'rxjs/Subject';
// services
import {ApiError} from '../../services/error.service';
// models
// configs
import {getApiConfig, IApiConfig} from '../../configs/api.config';


export namespace INote {

  export namespace Api {

    export type ObjectID = string;

    export type Response = INote.Api.Item[];

    export interface Item {
      _id: ObjectID;
      AppID: ObjectID;
      VersionNumber: number;
      ReleaseDate: Date;
      ReleaseNotes: string;
      Published: boolean;
    }

    export interface RequestNew {
      AppID: ObjectID;
      VersionNumber: number;
      ReleaseDate: Date;
      ReleaseNotes: string;
      Published: boolean;
    }

    export interface RequestUpdate {
      _id: ObjectID;
      Published: boolean;
    }
  }

  export namespace Options {

    export type RequestMode = 'getNotes' | 'newNote' | 'updateNote';

    export interface UpdateNoteMode {
      noteID: any;
      ReleaseNotes?: any;
      Published?: any;
    }

    export interface NewAppModes {
      VersionNumber: any;
      ReleaseDate: any;
      ReleaseNotes: any;
    }

    export type FieldType = 'AppID' | 'VersionNumber' | 'ReleaseDate' | 'ReleaseNotes' | 'Published';

    export const $FieldList: FieldType[] = ['AppID', 'VersionNumber', 'ReleaseDate', 'ReleaseNotes', 'Published'];

    export interface FieldNames {
      AppID: string;
      VersionNumber: string;
      ReleaseDate: string;
      ReleaseNotes: string;
      Published: string;
    }
  }

  export namespace Data {

    export interface WatcherApiUpdates {
      model: IApiConfig.Item;
      updates: Subject<INote.Data.WatcherApi>;
    }

    export interface WatcherApi {
      mode: INote.Options.RequestMode;
      query: string;
      body: null | INote.Api.RequestNew | INote.Api.RequestUpdate;
      cb: WatcherApiCb;
    }

    export type WatcherApiCb = (err: null | string, data: INote.Api.Response) => void;
  }

  export namespace Storage {

    export interface Internal {
      data: Data;
      configs: Configs;
      updates: Updates;
    }

    interface Data {
      activeUser: string;
      noteSet: {
        [appID: string]: INote.Api.Item[];
      };
    }

    interface Configs {
      _model: IApiConfig.Item;
    }

    interface Updates {
      api: Subject<INote.Data.WatcherApi>;
    }
  }
}

const _Internal: INote.Storage.Internal = {
  data: {
    activeUser: null,
    noteSet: {},
  },
  configs: {
    _model: getApiConfig().models.release.note,
  },
  updates: {
    api: new Subject<INote.Data.WatcherApi>(),
  },
};

export class Note {

  static load(appID: string, cb: (tracks: INote.Api.Item[]) => void): void {
    if (_Internal.data.noteSet[appID] !== void 0) {
      return cb(_Internal.data.noteSet[appID]);
    }

    NoteUtils.initApi('getNotes', appID, null, (err: null | string, data: INote.Api.Response) => {
      if (err !== null) {
        return new ApiError([err]);
      }

      _Internal.data.noteSet[appID] = data;
      cb(data);
    });
  }

  static sendNew(item: INote.Api.RequestNew) {
    NoteUtils.initApi('newNote', null, item, (err: null | string) => {
    });
  }

  static sendUpdate(item: INote.Api.RequestUpdate) {
    NoteUtils.initApi('updateNote', null, item, (err: null | string) => {
    });
  }

  static getSubscriptionApi(): INote.Data.WatcherApiUpdates {
    return {
      model: _Internal.configs._model,
      updates: _Internal.updates.api,
    };
  }
}

class NoteUtils {

  static initApi(mode: INote.Options.RequestMode,
                 appID: null | string,
                 item: null | INote.Api.RequestNew | INote.Api.RequestUpdate,
                 cb: INote.Data.WatcherApiCb) {
    _Internal.updates.api.next({
      mode,
      query: mode === 'getNotes' ? item.toString() : '',
      body: item,
      cb: (err: null | string, data: INote.Api.Response) => cb(err, data)
    });
  }

  static clear() {
    _Internal.data.noteSet = {};
  }
}
