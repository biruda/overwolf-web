'use strict';
// modules
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MatNativeDateModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// services
import {StorageService} from './_system/services/storage.service';
// models
// import {User, UserAuth} from './_system/models/admin';
// routes
import {AppRoutingModule} from './app-routing.module';
// apps' modules and main component
import {AppComponent} from './app.component';
import {PagesModule} from './pages/pages.module';
import {ComponentsModule} from './components/components.module';
import {TemplatesModule} from './templates/templates.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, MatNativeDateModule,
    AppRoutingModule,
    PagesModule, ComponentsModule, TemplatesModule,
  ],
  providers: [
    StorageService,
    // User, UserAuth
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
