'use strict';
// modules
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {NavigationEnd, Router} from '@angular/router';
// services
import {ApiService} from './_system/services/api.service';
import {RoutingService} from './_system/services/routing.service';
// models
import {IUser, User} from './_system/models/users/User';
import {INote, Note} from './_system/models/release/Note';
import {App, IApp} from './_system/models/apps/App';
// configs
import {getUrlConfig, IUrlConfig} from './_system/configs/url.config';
import {getNotifyConfig, INotifyConfig} from './_system/configs/notify.config';
import {getMainConfig, IMainConfig} from './_system/configs/main.config';
import {getAnalyticConfig, IAnalyticConfig} from './_system/configs/analytic.configs';

namespace IAppComponent {

  export interface Configs {
    _mainConfig: IMainConfig.Config;
    _notifyConfig: INotifyConfig.Config;
    _analyticConfig: IAnalyticConfig.Config;
    _urlConfig: IUrlConfig.Config;
    isDataGot: boolean;
    isTokenRequested: boolean;
    isAuth: boolean;
  }

  export interface Data {
    watchers: {
      router: Subscription,
      routing: Subscription,
      User: Subscription,
      Note: Subscription,
      App: Subscription,
    };
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public configs: IAppComponent.Configs = {
    _mainConfig: getMainConfig(),
    _notifyConfig: getNotifyConfig(),
    _analyticConfig: getAnalyticConfig(),
    _urlConfig: getUrlConfig(),
    isDataGot: null,
    isTokenRequested: null,
    isAuth: false,
  };
  public data: IAppComponent.Data = {
    watchers: {
      router: null,
      routing: null,
      User: null,
      Note: null,
      App: null,
    },
  };

  constructor(private _router: Router,
              private _api: ApiService) {
    // check auth params
    this.authAdminCheck();
    this.watchRouting();

    // add watcher to routing
    this.data.watchers.router = this._router.events.subscribe((event) => this.routerWatch(event));

    // watch to logout
    User.getSubscriptionSignOut().subscribe(() => {
      this.authAdminSignOut();
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.unSubscribe();
  }

  unSubscribe() {
    if (this.configs.isDataGot === false) {
      return;
    }
    this.configs.isDataGot = false;

    if (this.data.watchers.router !== null) {
      this.data.watchers.router.unsubscribe();
      this.data.watchers.router = null;
    }

    // api watchers

    if (this.data.watchers.User !== null) {
      this.data.watchers.User.unsubscribe();
      this.data.watchers.User = null;
    }
    if (this.data.watchers.App !== null) {
      this.data.watchers.App.unsubscribe();
      this.data.watchers.App = null;
    }
    if (this.data.watchers.Note !== null) {
      this.data.watchers.Note.unsubscribe();
      this.data.watchers.Note = null;
    }
  }

  // auth manipulation

  authAdminCheck(): void {
    const token: string | null = User.getToken();
    if (token === null) {
      this.authAdminSignOut();
      return;
    } else {
      this.configs.isAuth = true;
      this.__get();
    }
  }

  authAdminSignOut(): void {
    if (this.configs.isTokenRequested === true) {
      return;
    }
    this.configs.isTokenRequested = true;

    // signOut
    this.configs.isAuth = false;
    this.unSubscribe();

    // signIn
    this.goToLogin();
    if (this.data.watchers.User === null) {
      this.data.watchers.User = User.getSubscriptionSignIn().subscribe((info: IUser.Info.DataSignIn) => {
        this.data.watchers.User.unsubscribe();
        this.data.watchers.User = null;
        this.configs.isTokenRequested = false;
        if (info !== true) {
          return;
        }

        this.configs.isAuth = true;
        this.__get();
      });
    }
  }

  // router manipulation

  routerWatch = (event) => {
    // onLoad
    if (event instanceof NavigationEnd) {
      // check auth before redirect
      this.authAdminCheck();
    }
  }

  // init models get

  __get() {
    if (this.configs.isDataGot === true) {
      return;
    }
    this.configs.isDataGot = true;

    // api watchers
    this.getApiWatchers();
  }

  getApiWatchers(): void {

    // todo

    // if (this.data.watchers.User === null) {
    //   const watcherInfo: IUser.Data.WatcherApiUpdates = User.getSubscriptionApi();
    //   this.data.watchers.User = watcherInfo.updates.subscribe((params: IUser.Data.WatcherApi) => {
    //     this._api.get(watcherInfo.model, params.query, params.cb);
    //   });
    // }
    if (this.data.watchers.Note === null) {
      const watcherInfo: INote.Data.WatcherApiUpdates = Note.getSubscriptionApi();
      this.data.watchers.Note = watcherInfo.updates.subscribe((params: INote.Data.WatcherApi) => {

        switch (params.mode) {
          case 'getNotes':
            this._api.get(watcherInfo.model, params.query, params.cb);
            break;
          case 'newNote':
            this._api.put(watcherInfo.model, params.query, params.body, params.cb);
            break;
          case 'updateNote':
            this._api.post(watcherInfo.model, params.query, params.body, params.cb);
            break;

        }

      });
    }
    if (this.data.watchers.App === null) {
      const watcherInfo: IApp.Data.WatcherApiUpdates = App.getSubscriptionApi();
      this.data.watchers.App = watcherInfo.updates.subscribe((params: IApp.Data.WatcherApi) => {
        if (params.body !== null) {
          this._api.put(watcherInfo.model, params.query, params.body, params.cb);
        } else {
          this._api.get(watcherInfo.model, params.query, params.cb);
        }
      });
    }
  }

  // routing

  goToLogin() {
    const hashPairs = window.location.hash.slice(1).split('&');
    const params = {};
    for (let i = 0, len = hashPairs.length; i < len; i++) {

      const pair = hashPairs[i];
      const [key, value] = pair.split('=');

      params[key] = value;
    }
    User.setHashParams(params);

    // this._router.navigate(['/' + this.configs._urlConfig.adminGroup.authSignIn.url]).then();
    if (window.location.pathname.slice(1) !== this.configs._urlConfig.usersGroup.sign.url) {
      window.location.href = this.configs._urlConfig.usersGroup.sign.url;
    } else {
      this._router.navigate(['/' + this.configs._urlConfig.usersGroup.sign.url]).then();
    }
  }

  watchRouting() {
    if (this.data.watchers.routing === null) {
      this.data.watchers.routing = RoutingService.getAppSubscription()
        .subscribe((route: string) => {
          this._router.navigate([route]).then();
        });
    }
  }
}
