'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatFormFieldModule, MatInputModule, MatSelectModule} from '@angular/material';
// components
import {TableMainComponent} from './main/table-main.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule
  ],
  exports: [TableMainComponent],
  declarations: [TableMainComponent]
})
export class ComponentsTableModule {
}
