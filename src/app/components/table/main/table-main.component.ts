'use strict';
// modules
import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';


export namespace ITableMain {

  export interface Input {
    fields: string[];
    fieldNames: { [key: string]: string };
    items: object[];
  }

  export interface Output {
    ind: number;
    field: string;
    item: object;
  }

  export interface Configs {
    showCntOptions: number[];
    sorting: { [field: string]: boolean };
    pages: Array<string | number>;
    pagesLimit: number;
    showCnt: number;
    start: number;
    end: number;
    currentPage: number;
    isFilter: boolean;
    filter: { [field: string]: string };
  }

  export interface Data {
    itemsUnfiltered: object[];
    itemsSource: object[];
    items: object[];
    fields: string[];
    fieldNames: { [key: string]: string };
    fieldsHover: string[];
    fieldsHoverSet: { [field: string]: string };
    filterSelector: { [field: string]: string[] };
  }
}

@Component({
  selector: 'app-elements-table-main',
  templateUrl: './table-main.component.html',
  styleUrls: ['./table-main.component.scss']
})
export class TableMainComponent implements OnChanges {
  public configs: ITableMain.Configs = {
    showCntOptions: [10, 25, 50, 100],
    sorting: {},
    pages: [1],
    pagesLimit: 9,
    showCnt: 10,
    start: 1,
    end: 10,
    currentPage: 1,
    isFilter: false,
    filter: {},
  };
  public data: ITableMain.Data = {
    itemsUnfiltered: [],
    items: [],
    itemsSource: [],
    fields: [],
    fieldNames: {},
    fieldsHover: [],
    fieldsHoverSet: {},
    filterSelector: {},
  };

  @Input() public options: ITableMain.Input;
  @Output() public values = new EventEmitter<ITableMain.Output>();

  ngOnChanges() {
    this.data.itemsSource = this.options.items;
    this.data.fields = this.options.fields;
    this.data.fieldNames = this.options.fieldNames;
    this.configs.sorting = {};

    if (this.options.items[0] !== void 0) {
      this.prepareTooltipCell();
    }
    this.paginate();
  }

  prepareTooltipCell() {
    for (let i = 0, keys = Object.keys(this.options.items[0]), len = keys.length; i < len; i++) {
      const key = keys[i];
      const keySum = key + 'Sum';

      if (this.data.fields.indexOf(key) !== -1 && this.data.fields.indexOf(keySum) === -1) {
        continue;
      }

      this.data.fieldsHover.push(keySum);
      this.data.fieldsHoverSet[keySum] = key;
    }
  }

  setSorting(field: string): void {
    const _prev = this.configs.sorting[field];

    this.configs.sorting = {};
    this.configs.sorting[field] =
      _prev === void 0
        ? true
        : !_prev;

    const isAsc = this.configs.sorting[field];
    this.data.itemsSource = this.data.itemsSource.sort((a, b) => {
      return isAsc
        ? (a[field] < b[field] ? 1 : -1)
        : (a[field] > b[field] ? 1 : -1);
    });
    this.paginate();
  }

  // filter

  showFilter(): void {
    if (this.configs.isFilter) {
      this.data.itemsSource = this.data.itemsUnfiltered;
      this.configs.filter = {};
    } else {
      this.prepareFilterSelector();
    }

    this.configs.isFilter = !this.configs.isFilter;
    this.paginate();
  }

  removeFilter(field: string): void {
    delete this.configs.filter[field];
    this.filter();
  }

  prepareFilterSelector() {
    const infoSet: { [field: string]: { [value: string]: number } } = {};
    const fieldList: string[] = [];
    const itemsCnt = this.data.itemsSource.length;
    for (let i = 0; i < itemsCnt; i++) {
      const item = this.data.itemsSource[i];

      for (let j = 0, fields = Object.keys(item), lenFields = fields.length; j < lenFields; j++) {
        const field = fields[j];
        if (fieldList.indexOf(field) === -1) {
          fieldList.push(field);
        }

        const value = item[field];

        if (infoSet[field] === void 0) {
          infoSet[field] = {};
        }
        if (infoSet[field][value] === void 0) {
          infoSet[field][value] = 0;
        }

        infoSet[field][value]++;
      }
    }

    const selectedInfoSet: { [field: string]: string[] } = {};
    for (let i = 0, len = fieldList.length; i < len; i++) {
      const field = fieldList[i];
      selectedInfoSet[field] = [];

      const valueSet: { [value: string]: number } = infoSet[field];
      for (let j = 0, valueList = Object.keys(valueSet), lenValue = valueList.length; j < lenValue; j++) {
        const value = valueList[j];
        const valueCount = valueSet[value];

        if (valueCount / itemsCnt > 0.1) {
          selectedInfoSet[field].push(value);
        }
      }
    }

    this.data.filterSelector = selectedInfoSet;
  }

  filter(fieldSelect?: string, valueSelect?: string): void {
    if (valueSelect !== void 0) {
      this.configs.filter[fieldSelect] = valueSelect;
    }

    if (this.data.itemsUnfiltered[0] === void 0) {
      this.data.itemsUnfiltered = this.data.itemsSource;
    } else {
      this.data.itemsSource = this.data.itemsUnfiltered;
    }

    let _ItemsSource: object[] = Object.assign([], this.data.itemsSource);
    let _Items: object[] = [];
    for (let a = 0, fieldList: string[] = Object.keys(this.configs.filter), lenA = fieldList.length; a < lenA; a++) {
      const field: string = fieldList[a];
      for (let i = 0, len = _ItemsSource.length; i < len; i++) {
        const item: object = _ItemsSource[i];
        if (valueSelect !== void 0 && fieldSelect === field) {
          if (item[field].toString() === valueSelect.toString()) {
            _Items.push(item);
          }

          continue;
        }

        if (item[field].toString().indexOf(this.configs.filter[field]) !== -1) {
          _Items.push(item);
        }
      }

      _ItemsSource = Object.assign([], _Items);
      _Items = [];
    }

    this.data.itemsSource = _ItemsSource;
    this.paginate();
    this.prepareFilterSelector();

    const fieldSortList = Object.keys(this.configs.sorting);
    if (fieldSortList[0] !== void 0) {
      const field = fieldSortList[0];
      this.configs.sorting[field] = !this.configs.sorting[field];
      this.setSorting(field);
    }
  }

  // pagination

  setShowCnt() {
    this.paginate();
    this.setPage(this.configs.currentPage, null);
  }

  preparePagination() {
    const prePages: number[] = [];
    for (let i = 0, len = Math.ceil(this.data.itemsSource.length / this.configs.showCnt); i < len; i++) {
      prePages.push(i + 1);
    }
    const preLen = prePages.length;
    if (this.configs.currentPage > prePages[preLen - 1]) {
      this.configs.currentPage = prePages[preLen - 1];
    }

    let pages: Array<string | number> = [];
    if (this.configs.pagesLimit < preLen) {
      const half = Math.ceil(this.configs.pagesLimit / 2);
      if (this.configs.currentPage < half) {
        pages = prePages.slice(0, this.configs.pagesLimit - 1);
        pages.push('...');
      } else if (this.configs.currentPage > preLen - half) {
        pages = prePages.slice(preLen - this.configs.pagesLimit + 1);
        pages.unshift('...');
      } else {
        pages = prePages.slice(this.configs.currentPage - half + 1, this.configs.currentPage + half - 1);
        pages.unshift('...');
        pages.push('...');
      }
    } else {
      pages = prePages;
    }

    this.configs.pages = pages;
  }

  paginate() {
    this.preparePagination();
    this.data.items = this.data.itemsSource.slice(this.configs.start - 1, this.configs.end);
  }

  // routing

  goNext() {
    if (this.configs.currentPage === this.configs.pages[this.configs.pages.length - 1]) {
      return;
    }

    this.setPage(this.configs.currentPage + 1, null);
  }

  goPrevious() {
    if (this.configs.currentPage === 1) {
      return;
    }

    this.setPage(this.configs.currentPage - 1, null);
  }

  setPage(pageNumber: '...' | number, ind: number) {
    let page: number;
    if (pageNumber === '...') {
      if (ind === 0) {
        page = Number(this.configs.pages[1]) - 1;
      } else {
        page = Number(this.configs.pages[this.configs.pages.length - 2]) + 1;
      }
    } else {
      page = Number(pageNumber);
    }

    this.configs.currentPage = page;
    this.configs.end = page * this.configs.showCnt;
    if (this.configs.end > this.data.itemsSource.length) {
      this.configs.end = this.data.itemsSource.length;
    }

    this.configs.start = this.configs.end - this.configs.showCnt + 1;
    this.paginate();
  }

  // event

  clickItem(ind: number, field: string, item: object): void {
    this.values.emit({ind, field, item});
  }
}
