import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ControlInputCheckboxComponent} from './control-input-checkbox.component';

describe('ControlInputCheckboxComponent', () => {
  let component: ControlInputCheckboxComponent;
  let fixture: ComponentFixture<ControlInputCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ControlInputCheckboxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlInputCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
