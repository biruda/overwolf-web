'use strict';
// modules
import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';


export namespace IControlInputCheckboxComponent {

  export interface Input {
    name: string;
    result: string[] | null;
    items: {
      value: string;
      title: string;
      checked?: boolean;
    }[];
    errors: string[];
    isChecked: boolean;
  }

  export type Output = string[];

  export interface Data {
    values: string[];
    errors: string[];
  }
}

@Component({
  selector: 'app-elements-control-input-checkbox',
  templateUrl: './control-input-checkbox.component.html',
  styleUrls: ['./control-input-checkbox.component.scss']
})
export class ControlInputCheckboxComponent implements OnChanges {
  public data: IControlInputCheckboxComponent.Data = {
    values: [],
    errors: [],
  };

  @Input() public options: IControlInputCheckboxComponent.Input;
  @Output() public values: EventEmitter<IControlInputCheckboxComponent.Output> = new EventEmitter<IControlInputCheckboxComponent.Output>();

  ngOnChanges() {
    this.data.values = this.options.result !== null
      ? Object.assign([], this.options.result)
      : [];
    this.data.errors = this.options.errors;
  }

  onPress(value: string) {
    const ind = this.data.values.indexOf(value);
    if (ind === -1) {
      this.data.values.push(value);
    } else {
      this.data.values.splice(ind, 1);
    }
    for (let i = 0, len = this.options.items.length; i < len; i++) {
      if (this.options.items[i].value === value) {
        this.options.items[i].checked = ind === -1;
        break;
      }
    }

    this.options.isChecked = false;
    this.data.errors = [];
    this.values.emit(this.data.values);
  }
}
