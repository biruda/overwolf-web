'use strict';
// modules
import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, Renderer2} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';

export namespace IControlInputDropdownComponent {

  export interface Input {
    name: string;
    value: string;
    options: string[];
  }

  export interface Configs {
    isOpen: boolean;
    isFocus: boolean;
    isMouseleave: boolean;
  }

  export interface Data {
    value: string;
    chosen: { [id: string]: boolean };
    chosenList: string[];
    watchers: {
      windowClick: any,
      lang: Subscription,
    };
  }
}

@Component({
  selector: 'app-elements-control-input-dropdown',
  templateUrl: './control-input-dropdown.component.html',
  styleUrls: ['./control-input-dropdown.component.scss']
})
export class ControlInputDropdownComponent implements OnInit, OnChanges, OnDestroy {
  public configs: IControlInputDropdownComponent.Configs = {
    isOpen: false,
    isFocus: false,
    isMouseleave: null,
  };
  public data: IControlInputDropdownComponent.Data = {
    value: '',
    chosen: {},
    chosenList: [],
    watchers: {
      lang: null,
      windowClick: () => 0
    },
  };

  @Input() public options: IControlInputDropdownComponent.Input;
  @Output() public values = new EventEmitter<string>();
  @Output() public start = new EventEmitter<string>();

  constructor(private _renderer: Renderer2) {
  }

  /**
   * @function ngOnInit
   * @description event handler for component ready
   */
  ngOnInit() {
    // hide list when click outside
    this.data.watchers.windowClick = this._renderer.listen('window', 'click', () => {
      if (this.configs.isMouseleave === true) {
        this.configs.isFocus = this.data.value !== '';
      }
    });
  }

  ngOnDestroy() {
    if (this.data.watchers.lang !== null) {
      this.data.watchers.lang.unsubscribe();
      this.data.watchers.lang = null;
    }
    if (this.data.watchers.windowClick !== null) {
      this.data.watchers.windowClick();
      this.data.watchers.windowClick = null;
    }
  }

  ngOnChanges() {
    if (this.data.value !== this.options.value) {
      this.data.chosenList = [];
      this.data.chosen = {};
    }

    this.data.value = this.options.value;
    this.configs.isFocus = this.data.value !== '' && this.data.value !== null;
  }

  optionInit() {
    this.configs.isFocus = this.data.value !== '' ? true : !this.configs.isFocus;
  }

  optionChoose(i: string, noUpdate?: boolean) {
    this.data.value = this.options.options[i];
    this.configs.isFocus = true;

    // mark to class
    if (this.data.chosen[i] === void 0) {
      this.data.chosen = {};
      this.data.chosen[i] = true;
    } else {
      const isChosen = !this.data.chosen[i];
      this.data.chosen = {};
      this.data.chosen[i] = isChosen;
    }

    if (noUpdate !== true) {
      this.values.emit(this.data.value);
    }
  }

  startWrite() {
    this.configs.isFocus = this.data.value !== '' ? true : !this.configs.isFocus;
    this.start.emit(this.data.value);
  }

  toggleActive() {
    this.configs.isOpen = !this.configs.isOpen;
  }
}
