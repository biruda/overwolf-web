import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ControlInputDropdownComponent} from './control-input-dropdown.component';

describe('ControlInputDropdownComponent', () => {
  let component: ControlInputDropdownComponent;
  let fixture: ComponentFixture<ControlInputDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ControlInputDropdownComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlInputDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
