'use strict';
// modules
import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';


export namespace IControlInput {

  export interface Input {
    title: string;
    value: string;
    classes: string[];
    errors: string[];
    isDisabled?: boolean;
    isChecked: boolean;
    isStrictValue?: boolean;
    type?: string;
  }

  export type Output = string;

  export interface Configs {
    isFocus: boolean;
    classes: string[];
  }

  export interface Data {
    value: string;
    errors: Array<string>;
  }
}

@Component({
  selector: 'app-elements-control-input',
  templateUrl: './control-input.component.html',
  styleUrls: ['./control-input.component.scss']
})
export class ControlInputComponent implements OnChanges {
  public configs: IControlInput.Configs = {
    isFocus: false,
    classes: [],
  };
  public data: IControlInput.Data = {
    value: '',
    errors: [],
  };

  @Input() public options: IControlInput.Input;
  @Output() public values: EventEmitter<IControlInput.Output> = new EventEmitter<IControlInput.Output>();

  private prepareClasses() {
    this.configs.classes = this.options.classes !== void 0 ? Object.assign([], this.options.classes) : [];
    if (this.configs.isFocus === true) {
      this.configs.classes.push('focused');
    }
    if (this.data.errors.length > 0) {
      this.configs.classes.push('error');
    }
    if (this.data.errors.length === 0 && this.options.isChecked === true) {
      this.configs.classes.push('success');
    }
  }

  ngOnChanges() {
    if (this.options.isStrictValue === true) {
      this.data.value = this.options.value;
    } else if (this.data.value === '' || this.options.errors.length !== 0 || this.options.isChecked === true) {
      this.data.value = this.options.value;
    }
    this.data.errors = this.options.errors;

    this.configs.isFocus = this.data.value !== '' && this.data.value !== null;
    this.prepareClasses();
  }

  onPress(event: Event) {
    if (event.isTrusted === false) {
      return;
    }

    this.options.isChecked = false;
    this.data.errors = [];
    this.prepareClasses();
    this.values.emit(this.data.value);
  }

  onFocus(event: FocusEvent): void {
    if (event.isTrusted !== true) {
      return;
    }

    if (event.type === 'focus') {
      this.configs.isFocus = true;
    } else if (this.data.value === '' || this.data.value === null) {
      this.configs.isFocus = false;
    }
    this.prepareClasses();
  }
}
