'use strict';
// modules
import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';


export namespace IControlDatepickerComponent {

  export interface Input {
    title: string;
    value?: string;
    minDate?: string;
    maxDate?: string;
    errors: string[];
    isChecked: boolean;
  }

  export type Output = string;

  export interface Data {
    startDate: Date;
    resultDate: Date;
    errors: string[];
  }

  export interface Configs {
    minDate: Date;
    maxDate: Date;
  }
}

@Component({
  selector: 'app-elements-control-datepicker',
  templateUrl: './control-datepicker.component.html',
  styleUrls: ['./control-datepicker.component.scss']
})
export class ControlDatepickerComponent implements OnChanges {
  public data: IControlDatepickerComponent.Data = {
    startDate: new Date(),
    resultDate: null,
    errors: [],
  };
  public configs: IControlDatepickerComponent.Configs = {
    minDate: null,
    maxDate: null,
  };

  @Input() public options: IControlDatepickerComponent.Input;
  @Output() public values: EventEmitter<IControlDatepickerComponent.Output> = new EventEmitter<IControlDatepickerComponent.Output>();

  ngOnChanges() {
    this.data.resultDate =
      this.options.value !== void 0 && this.options.value !== null
        ? new Date(this.options.value)
        : null;
    this.data.errors = this.options.errors;

    if (this.configs.minDate === null) {
      const d = new Date();
      d.setUTCFullYear(d.getUTCFullYear() - 5);
      this.configs.minDate = this.options.minDate !== void 0 ? new Date(this.options.minDate) : d;
    }
    if (this.configs.maxDate === null) {
      const d = new Date();
      d.setUTCFullYear(d.getUTCFullYear() + 5);
      this.configs.maxDate = this.options.maxDate !== void 0 ? new Date(this.options.maxDate) : d;
    }
  }

  onChange() {
    this.options.isChecked = false;
    this.data.errors = [];
    this.values.emit(this.data.resultDate.toISOString().slice(0, 10));
  }
}
