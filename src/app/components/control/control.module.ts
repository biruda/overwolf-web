'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {CdkTableModule} from '@angular/cdk/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
// components
import {ControlInputComponent} from './input/control-input.component';
import {ControlInputRadioComponent} from './input-radio/control-input-radio.component';
import {ControlInputCheckboxComponent} from './input-checkbox/control-input-checkbox.component';
import {ControlInputDropdownComponent} from './input-dropdown/control-input-dropdown.component';
import {ControlDatepickerComponent} from './datepicker/control-datepicker.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CdkTableModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
  ],
  exports: [
    ControlInputComponent, ControlInputRadioComponent, ControlInputCheckboxComponent, ControlInputDropdownComponent, ControlDatepickerComponent
  ],
  declarations: [
    ControlInputComponent, ControlInputRadioComponent, ControlInputCheckboxComponent, ControlInputDropdownComponent, ControlDatepickerComponent
  ]
})
export class ComponentsControlModule {
}
