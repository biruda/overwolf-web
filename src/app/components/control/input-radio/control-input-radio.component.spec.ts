import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlInputRadioComponent} from './control-input-radio.component';

describe('ControlInputRadioComponent', () => {
  let component: ControlInputRadioComponent;
  let fixture: ComponentFixture<ControlInputRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlInputRadioComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlInputRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
