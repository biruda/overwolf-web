'use strict';
// modules
import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';


export namespace IControlInputRadioComponent {

  export interface Input {
    name: string;
    result: string | null;
    items: {
      value: string;
      title: string;
    }[];
    errors: string[];
    isChecked: boolean;
  }

  export type Output = string;

  export interface Data {
    value: string;
    errors: string[];
  }
}

@Component({
  selector: 'app-elements-control-input-radio',
  templateUrl: './control-input-radio.component.html',
  styleUrls: ['./control-input-radio.component.scss']
})
export class ControlInputRadioComponent implements OnChanges {
  public data: IControlInputRadioComponent.Data = {
    value: '',
    errors: [],
  };

  @Input() public options: IControlInputRadioComponent.Input;
  @Output() public values: EventEmitter<IControlInputRadioComponent.Output> = new EventEmitter<IControlInputRadioComponent.Output>();

  ngOnChanges() {
    this.data.value = this.options.result !== null ? this.options.result : '';
    this.data.errors = this.options.errors;
  }

  onPress() {
    this.options.isChecked = false;
    this.data.errors = [];
    this.values.emit(this.data.value);
  }
}
