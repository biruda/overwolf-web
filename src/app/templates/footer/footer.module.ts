'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
// components
import {MainComponent} from './main/main.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [MainComponent],
  declarations: [MainComponent]
})
export class FooterModule {
}
