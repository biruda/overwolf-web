'use strict';
// modules
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
// configs
import {getMainConfig, IMainConfig} from '../../../_system/configs/main.config';

namespace IMainComponent {

  export interface Configs {
    readonly _mainConfig: IMainConfig.Config;
  }

  export interface Data {
    watchers: {
      lang: Subscription
    };
  }
}

@Component({
  selector: 'app-templates-footer-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  public lang: IMainConfig.LangField;
  public configs: IMainComponent.Configs = {
    _mainConfig: getMainConfig(),
  };
  public data: IMainComponent.Data = {
    watchers: {
      lang: null
    },
  };

  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.data.watchers.lang !== null) {
      this.data.watchers.lang.unsubscribe();
      this.data.watchers.lang = null;
    }
  }
}
