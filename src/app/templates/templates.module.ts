'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
// components
import {HeaderModule} from './header/header.module';
import {FooterModule} from './footer/footer.module';
import {SidebarModule} from './sidebar/sidebar.module';

// models

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    FooterModule,
    SidebarModule,
  ],
  exports: [
    HeaderModule,
    FooterModule,
    SidebarModule,
  ],
  providers: [],
  declarations: []
})
export class TemplatesModule {
}
