'use strict';
// modules
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
// services
import {ClientError} from '../../../_system/services/error.service';
import {RoutingService} from '../../../_system/services/routing.service';
// configs
import {IMainConfig} from '../../../_system/configs/main.config';
import {getUrlConfig, IUrlConfig} from '../../../_system/configs/url.config';

export namespace IMainComponent {

  export interface Configs {
    _urlConfig: IUrlConfig.Config;
    expandedGroups: {
      moderationGroup: boolean;
      statisticsGroup: boolean;
      notificationsGroup: boolean;
      boosterGroup: boolean;
    };
    activeGroup: string;
    activeLink: string;
    decodedGroups: {
      appsGroup: string;
      releaseGroup: string;
    };
    decodedLinks: {
      appsGroup: {
        appList: string;
      };
      releaseGroup: {
        note: string;
      };
    };
  }

  export interface Watchers {
    UserModerationProfile: Subscription;
    AboutMeModeration: Subscription;
    PhotoModeration: Subscription;
  }
}

@Component({
  selector: 'app-templates-sidebar-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  public lang: IMainConfig.LangField;
  public configs: IMainComponent.Configs = {
    _urlConfig: getUrlConfig(),
    expandedGroups: {
      moderationGroup: true,
      statisticsGroup: false,
      notificationsGroup: false,
      boosterGroup: false,
    },
    activeGroup: null,
    activeLink: null,
    decodedGroups: {
      appsGroup: 'apps',
      releaseGroup: 'release',
    },
    decodedLinks: {
      appsGroup: {
        appList: 'appList',
      },
      releaseGroup: {
        note: 'note',
      },
    },
  };
  public watchers: IMainComponent.Watchers = {
    UserModerationProfile: null,
    AboutMeModeration: null,
    PhotoModeration: null,
  };

  constructor() {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.watchers.UserModerationProfile !== null) {
      this.watchers.UserModerationProfile.unsubscribe();
    }
    if (this.watchers.AboutMeModeration !== null) {
      this.watchers.AboutMeModeration.unsubscribe();
    }
    if (this.watchers.PhotoModeration !== null) {
      this.watchers.PhotoModeration.unsubscribe();
    }
  }

  // routing

  goToApps(route: string): void {
    this.configs.activeGroup = this.configs.decodedGroups.appsGroup;
    this.configs.activeLink = this.configs.decodedGroups.appsGroup + '-' + route;

    const routeItem: IUrlConfig.ConfigItem = this.configs._urlConfig.appGroup[route];
    if (routeItem === void 0) {
      const e = new ClientError(['bad params', 'not correct page to routing', route]);
      return;
    }

    RoutingService.setAppRender(routeItem.url);
  }

  goToRelease(route: string): void {
    this.configs.activeGroup = this.configs.decodedGroups.releaseGroup;
    this.configs.activeLink = this.configs.decodedGroups.releaseGroup + '-' + route;

    const routeItem: IUrlConfig.ConfigItem = this.configs._urlConfig.releaseGroup[route];
    if (routeItem === void 0) {
      const e = new ClientError(['bad params', 'not correct page to routing', route]);
      return;
    }

    RoutingService.setAppRender(routeItem.url);
  }
}
