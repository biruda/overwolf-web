'use strict';
// modules
import {Component} from '@angular/core';


@Component({
  selector: 'app-templates-header-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
}
