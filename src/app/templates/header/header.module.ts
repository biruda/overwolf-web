'use strict';
// modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
// components
import {MainComponent} from './main/main.component';

@NgModule({
  imports: [
    CommonModule, RouterModule
  ],
  exports: [MainComponent],
  declarations: [MainComponent]
})
export class HeaderModule {
}
